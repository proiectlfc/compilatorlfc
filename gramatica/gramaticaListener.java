// Generated from gramatica.g4 by ANTLR 4.9
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link gramaticaParser}.
 */
public interface gramaticaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#folosireVariabila}.
	 * @param ctx the parse tree
	 */
	void enterFolosireVariabila(gramaticaParser.FolosireVariabilaContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#folosireVariabila}.
	 * @param ctx the parse tree
	 */
	void exitFolosireVariabila(gramaticaParser.FolosireVariabilaContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#declarareVariabila}.
	 * @param ctx the parse tree
	 */
	void enterDeclarareVariabila(gramaticaParser.DeclarareVariabilaContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#declarareVariabila}.
	 * @param ctx the parse tree
	 */
	void exitDeclarareVariabila(gramaticaParser.DeclarareVariabilaContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#declarareFunctie}.
	 * @param ctx the parse tree
	 */
	void enterDeclarareFunctie(gramaticaParser.DeclarareFunctieContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#declarareFunctie}.
	 * @param ctx the parse tree
	 */
	void exitDeclarareFunctie(gramaticaParser.DeclarareFunctieContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#apelareFunctie}.
	 * @param ctx the parse tree
	 */
	void enterApelareFunctie(gramaticaParser.ApelareFunctieContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#apelareFunctie}.
	 * @param ctx the parse tree
	 */
	void exitApelareFunctie(gramaticaParser.ApelareFunctieContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#parsare}.
	 * @param ctx the parse tree
	 */
	void enterParsare(gramaticaParser.ParsareContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#parsare}.
	 * @param ctx the parse tree
	 */
	void exitParsare(gramaticaParser.ParsareContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#bloc}.
	 * @param ctx the parse tree
	 */
	void enterBloc(gramaticaParser.BlocContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#bloc}.
	 * @param ctx the parse tree
	 */
	void exitBloc(gramaticaParser.BlocContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#return_line}.
	 * @param ctx the parse tree
	 */
	void enterReturn_line(gramaticaParser.Return_lineContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#return_line}.
	 * @param ctx the parse tree
	 */
	void exitReturn_line(gramaticaParser.Return_lineContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#linie}.
	 * @param ctx the parse tree
	 */
	void enterLinie(gramaticaParser.LinieContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#linie}.
	 * @param ctx the parse tree
	 */
	void exitLinie(gramaticaParser.LinieContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#atribuire}.
	 * @param ctx the parse tree
	 */
	void enterAtribuire(gramaticaParser.AtribuireContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#atribuire}.
	 * @param ctx the parse tree
	 */
	void exitAtribuire(gramaticaParser.AtribuireContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#daca_parametrii}.
	 * @param ctx the parse tree
	 */
	void enterDaca_parametrii(gramaticaParser.Daca_parametriiContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#daca_parametrii}.
	 * @param ctx the parse tree
	 */
	void exitDaca_parametrii(gramaticaParser.Daca_parametriiContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#bloc_conditie}.
	 * @param ctx the parse tree
	 */
	void enterBloc_conditie(gramaticaParser.Bloc_conditieContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#bloc_conditie}.
	 * @param ctx the parse tree
	 */
	void exitBloc_conditie(gramaticaParser.Bloc_conditieContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#bloc_declaratii}.
	 * @param ctx the parse tree
	 */
	void enterBloc_declaratii(gramaticaParser.Bloc_declaratiiContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#bloc_declaratii}.
	 * @param ctx the parse tree
	 */
	void exitBloc_declaratii(gramaticaParser.Bloc_declaratiiContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#cat_timp_parametrii}.
	 * @param ctx the parse tree
	 */
	void enterCat_timp_parametrii(gramaticaParser.Cat_timp_parametriiContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#cat_timp_parametrii}.
	 * @param ctx the parse tree
	 */
	void exitCat_timp_parametrii(gramaticaParser.Cat_timp_parametriiContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#pentru_parametri}.
	 * @param ctx the parse tree
	 */
	void enterPentru_parametri(gramaticaParser.Pentru_parametriContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#pentru_parametri}.
	 * @param ctx the parse tree
	 */
	void exitPentru_parametri(gramaticaParser.Pentru_parametriContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#pentru_initializari}.
	 * @param ctx the parse tree
	 */
	void enterPentru_initializari(gramaticaParser.Pentru_initializariContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#pentru_initializari}.
	 * @param ctx the parse tree
	 */
	void exitPentru_initializari(gramaticaParser.Pentru_initializariContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#pentru_operatii}.
	 * @param ctx the parse tree
	 */
	void enterPentru_operatii(gramaticaParser.Pentru_operatiiContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#pentru_operatii}.
	 * @param ctx the parse tree
	 */
	void exitPentru_operatii(gramaticaParser.Pentru_operatiiContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#incrementari_decrementari}.
	 * @param ctx the parse tree
	 */
	void enterIncrementari_decrementari(gramaticaParser.Incrementari_decrementariContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#incrementari_decrementari}.
	 * @param ctx the parse tree
	 */
	void exitIncrementari_decrementari(gramaticaParser.Incrementari_decrementariContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#selectare_parametri}.
	 * @param ctx the parse tree
	 */
	void enterSelectare_parametri(gramaticaParser.Selectare_parametriContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#selectare_parametri}.
	 * @param ctx the parse tree
	 */
	void exitSelectare_parametri(gramaticaParser.Selectare_parametriContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#bloc_caz_selectare}.
	 * @param ctx the parse tree
	 */
	void enterBloc_caz_selectare(gramaticaParser.Bloc_caz_selectareContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#bloc_caz_selectare}.
	 * @param ctx the parse tree
	 */
	void exitBloc_caz_selectare(gramaticaParser.Bloc_caz_selectareContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#caz_implicit_selectare}.
	 * @param ctx the parse tree
	 */
	void enterCaz_implicit_selectare(gramaticaParser.Caz_implicit_selectareContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#caz_implicit_selectare}.
	 * @param ctx the parse tree
	 */
	void exitCaz_implicit_selectare(gramaticaParser.Caz_implicit_selectareContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#executa_pana_cand_parametri}.
	 * @param ctx the parse tree
	 */
	void enterExecuta_pana_cand_parametri(gramaticaParser.Executa_pana_cand_parametriContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#executa_pana_cand_parametri}.
	 * @param ctx the parse tree
	 */
	void exitExecuta_pana_cand_parametri(gramaticaParser.Executa_pana_cand_parametriContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(gramaticaParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(gramaticaParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link gramaticaParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(gramaticaParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link gramaticaParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(gramaticaParser.AtomContext ctx);
}