grammar gramatica;


folosireVariabila
: VARIABILA
;

declarareVariabila
 : TIP_DATA VARIABILA ATRIBUIRE expr
 | TIP_DATA VARIABILA
;

declarareFunctie
 : FUNCTIE VARIABILA PARANTEZA_D (TIP_DATA (DIEZ|AMPERSAND) VARIABILA)? PARANTEZA_I DOUA_PUNCTE TIP_DATA bloc
 ;
 
 apelareFunctie
 : VARIABILA PARANTEZA_D expr? PARANTEZA_I
 ;

INTREG
 : SEMN? CIFRA_NENULA CIFRA* | '0'
 ;

REAL
 : INTREG PUNCT CIFRA+ ('e' SEMN? CIFRA_NENULA CIFRA*)? 
 ;

parsare
 : linie* EOF
 ;

bloc
 : ACOLADA_D (linie)* ACOLADA_I
 ;

return_line
: RETURN expr? PUNCT_VIRGULA
;

linie
 : declarareVariabila PUNCT_VIRGULA
 | bloc
 | declarareFunctie
 | atribuire PUNCT_VIRGULA
 | incrementari_decrementari PUNCT_VIRGULA
 | daca_parametrii
 | executa_pana_cand_parametri
 | cat_timp_parametrii
 | pentru_parametri
 | apelareFunctie PUNCT_VIRGULA
 | selectare_parametri
 | return_line
 ;


atribuire
 : folosireVariabila ATRIBUIRE expr
 | folosireVariabila PLUS_ATR expr
 | folosireVariabila MINUS_ATR expr
 | folosireVariabila IMPARTIRE_ATR expr
 | folosireVariabila INMULTIRE_ATR expr
 ;

daca_parametrii
 : DACA bloc_conditie (ALTFEL DACA bloc_conditie)* (ALTFEL bloc_declaratii)?
 ;

bloc_conditie
 : expr bloc_declaratii
 ;

bloc_declaratii
 : bloc 
 | linie
 ;

cat_timp_parametrii
 : CAT_TIMP expr bloc
 ;

pentru_parametri
 : PENTRU PARANTEZA_D (pentru_initializari)* PUNCT_VIRGULA (expr)* PUNCT_VIRGULA (pentru_operatii)* PARANTEZA_I bloc
 ; 
 pentru_initializari
 : (atribuire|declarareVariabila)(VIRGULA)?
 ; 

 pentru_operatii
 : (incrementari_decrementari|atribuire) VIRGULA?
 ;

 incrementari_decrementari
 : folosireVariabila INCREMENTARE
 | folosireVariabila DECREMENTARE
 | INCREMENTARE folosireVariabila
 | DECREMENTARE folosireVariabila
 ;

selectare_parametri
 : SELECTARE folosireVariabila ACOLADA_D bloc_caz_selectare* caz_implicit_selectare? ACOLADA_I
 ;

bloc_caz_selectare
 : CAZ expr DOUA_PUNCTE linie* (INTRERUPE PUNCT_VIRGULA)?
 ;

caz_implicit_selectare
 : IMPLICIT DOUA_PUNCTE linie* (INTRERUPE PUNCT_VIRGULA)?
 ;
 
executa_pana_cand_parametri
 : EXECUTA bloc PANA_CAND expr PUNCT_VIRGULA
 ;

expr
 : MINUS expr
 | NU expr
 | PARANTEZA_D expr PARANTEZA_I
 | expr op=(INMULTIRE | IMPARTIRE| PROCENT) expr
 | expr op=(PLUS | MINUS) expr
 | expr op=(MIC_EGAL |MARE_EGAL| MIC | MARE) expr
 | expr op=(EGAL | DIFERIT) expr
 | expr SI expr
 | expr SAU expr
 | atom
 ;

atom
 : (INTREG | REAL)
 | folosireVariabila
 | apelareFunctie
 | CARACTER
 ;

fragment TIP_INT : 'intreg';
fragment TIP_REAL : 'real';
fragment TIP_CARACTER : 'caracter';
fragment TIP_SIR : 'sir';
fragment TIP_VID : 'vid';
fragment LITERA_MICA : [a-z];
fragment LITERA_MARE : [A-Z];
fragment LITERA : LITERA_MICA | LITERA_MARE;
fragment CIFRA : [0-9];
fragment CIFRA_NENULA : [1-9];
fragment SEMN : ('+'|'-');

RETURN: 'returneaza';
SAU : 'SAU';
SI : 'SI';
NU : 'NU';
EGAL : 'EGAL';
DIFERIT : 'DIFERIT';
MARE : '>';
MIC : '<';
MARE_EGAL : '>=';
MIC_EGAL : '<=';
INMULTIRE : '*';
IMPARTIRE : '/';
PLUS_ATR : '+=';
INCREMENTARE : '++';
MINUS_ATR : '-=';
DECREMENTARE : '--';
INMULTIRE_ATR : '*=';
IMPARTIRE_ATR : '/=';
PROCENT : '%';
PUTERE : '^';
PLUS : '+';
MINUS : '-';
AMPERSAND : '&';
DIEZ : '#';
PARANTEZA_D : '(';
PARANTEZA_I : ')';
ACOLADA_D : '{';
ACOLADA_I : '}';
DOUA_PUNCTE : ':';
PUNCT : '.';
ATRIBUIRE: '=';
VIRGULA : ',';
PUNCT_VIRGULA : ';';

DACA : 'DACA';
ALTFEL : 'ALTFEL';
PENTRU : 'PENTRU';
CAT_TIMP : 'CAT_TIMP';
EXECUTA : 'EXECUTA';
PANA_CAND : 'PANA_CAND';
SELECTARE : 'SELECTARE';
CAZ : 'CAZ';
IMPLICIT : 'IMPLICIT';
INTRERUPE : 'INTRERUPE';

FUNCTIE : 'FUNCTIE';

COMENTARIU: '...' ~[\r\n]* -> skip;

SPATIU: (' ' | '\t' | '\n') -> skip;

TIP_DATA 
: TIP_INT | TIP_REAL | TIP_CARACTER | TIP_SIR | TIP_VID
;

VARIABILA
 : LITERA (LITERA | CIFRA)* 
 ;

CARACTER : '\'' ~('\'') '\'';

SPACE
 : [ \t\r\n] -> skip
 ;
OTHER
 : . 
 ;

