// Generated from gramatica.g4 by ANTLR 4.9
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class gramaticaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		INTREG=1, REAL=2, RETURN=3, SAU=4, SI=5, NU=6, EGAL=7, DIFERIT=8, MARE=9, 
		MIC=10, MARE_EGAL=11, MIC_EGAL=12, INMULTIRE=13, IMPARTIRE=14, PLUS_ATR=15, 
		INCREMENTARE=16, MINUS_ATR=17, DECREMENTARE=18, INMULTIRE_ATR=19, IMPARTIRE_ATR=20, 
		PROCENT=21, PUTERE=22, PLUS=23, MINUS=24, AMPERSAND=25, DIEZ=26, PARANTEZA_D=27, 
		PARANTEZA_I=28, ACOLADA_D=29, ACOLADA_I=30, DOUA_PUNCTE=31, PUNCT=32, 
		ATRIBUIRE=33, VIRGULA=34, PUNCT_VIRGULA=35, DACA=36, ALTFEL=37, PENTRU=38, 
		CAT_TIMP=39, EXECUTA=40, PANA_CAND=41, SELECTARE=42, CAZ=43, IMPLICIT=44, 
		INTRERUPE=45, FUNCTIE=46, COMENTARIU=47, SPATIU=48, TIP_DATA=49, VARIABILA=50, 
		CARACTER=51, SPACE=52, OTHER=53;
	public static final int
		RULE_folosireVariabila = 0, RULE_declarareVariabila = 1, RULE_declarareFunctie = 2, 
		RULE_apelareFunctie = 3, RULE_parsare = 4, RULE_bloc = 5, RULE_return_line = 6, 
		RULE_linie = 7, RULE_atribuire = 8, RULE_daca_parametrii = 9, RULE_bloc_conditie = 10, 
		RULE_bloc_declaratii = 11, RULE_cat_timp_parametrii = 12, RULE_pentru_parametri = 13, 
		RULE_pentru_initializari = 14, RULE_pentru_operatii = 15, RULE_incrementari_decrementari = 16, 
		RULE_selectare_parametri = 17, RULE_bloc_caz_selectare = 18, RULE_caz_implicit_selectare = 19, 
		RULE_executa_pana_cand_parametri = 20, RULE_expr = 21, RULE_atom = 22;
	private static String[] makeRuleNames() {
		return new String[] {
			"folosireVariabila", "declarareVariabila", "declarareFunctie", "apelareFunctie", 
			"parsare", "bloc", "return_line", "linie", "atribuire", "daca_parametrii", 
			"bloc_conditie", "bloc_declaratii", "cat_timp_parametrii", "pentru_parametri", 
			"pentru_initializari", "pentru_operatii", "incrementari_decrementari", 
			"selectare_parametri", "bloc_caz_selectare", "caz_implicit_selectare", 
			"executa_pana_cand_parametri", "expr", "atom"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "'returneaza'", "'SAU'", "'SI'", "'NU'", "'EGAL'", 
			"'DIFERIT'", "'>'", "'<'", "'>='", "'<='", "'*'", "'/'", "'+='", "'++'", 
			"'-='", "'--'", "'*='", "'/='", "'%'", "'^'", "'+'", "'-'", "'&'", "'#'", 
			"'('", "')'", "'{'", "'}'", "':'", "'.'", "'='", "','", "';'", "'DACA'", 
			"'ALTFEL'", "'PENTRU'", "'CAT_TIMP'", "'EXECUTA'", "'PANA_CAND'", "'SELECTARE'", 
			"'CAZ'", "'IMPLICIT'", "'INTRERUPE'", "'FUNCTIE'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "INTREG", "REAL", "RETURN", "SAU", "SI", "NU", "EGAL", "DIFERIT", 
			"MARE", "MIC", "MARE_EGAL", "MIC_EGAL", "INMULTIRE", "IMPARTIRE", "PLUS_ATR", 
			"INCREMENTARE", "MINUS_ATR", "DECREMENTARE", "INMULTIRE_ATR", "IMPARTIRE_ATR", 
			"PROCENT", "PUTERE", "PLUS", "MINUS", "AMPERSAND", "DIEZ", "PARANTEZA_D", 
			"PARANTEZA_I", "ACOLADA_D", "ACOLADA_I", "DOUA_PUNCTE", "PUNCT", "ATRIBUIRE", 
			"VIRGULA", "PUNCT_VIRGULA", "DACA", "ALTFEL", "PENTRU", "CAT_TIMP", "EXECUTA", 
			"PANA_CAND", "SELECTARE", "CAZ", "IMPLICIT", "INTRERUPE", "FUNCTIE", 
			"COMENTARIU", "SPATIU", "TIP_DATA", "VARIABILA", "CARACTER", "SPACE", 
			"OTHER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "gramatica.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public gramaticaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class FolosireVariabilaContext extends ParserRuleContext {
		public TerminalNode VARIABILA() { return getToken(gramaticaParser.VARIABILA, 0); }
		public FolosireVariabilaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_folosireVariabila; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterFolosireVariabila(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitFolosireVariabila(this);
		}
	}

	public final FolosireVariabilaContext folosireVariabila() throws RecognitionException {
		FolosireVariabilaContext _localctx = new FolosireVariabilaContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_folosireVariabila);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(VARIABILA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarareVariabilaContext extends ParserRuleContext {
		public TerminalNode TIP_DATA() { return getToken(gramaticaParser.TIP_DATA, 0); }
		public TerminalNode VARIABILA() { return getToken(gramaticaParser.VARIABILA, 0); }
		public TerminalNode ATRIBUIRE() { return getToken(gramaticaParser.ATRIBUIRE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public DeclarareVariabilaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarareVariabila; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterDeclarareVariabila(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitDeclarareVariabila(this);
		}
	}

	public final DeclarareVariabilaContext declarareVariabila() throws RecognitionException {
		DeclarareVariabilaContext _localctx = new DeclarareVariabilaContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declarareVariabila);
		try {
			setState(54);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(48);
				match(TIP_DATA);
				setState(49);
				match(VARIABILA);
				setState(50);
				match(ATRIBUIRE);
				setState(51);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(52);
				match(TIP_DATA);
				setState(53);
				match(VARIABILA);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarareFunctieContext extends ParserRuleContext {
		public TerminalNode FUNCTIE() { return getToken(gramaticaParser.FUNCTIE, 0); }
		public List<TerminalNode> VARIABILA() { return getTokens(gramaticaParser.VARIABILA); }
		public TerminalNode VARIABILA(int i) {
			return getToken(gramaticaParser.VARIABILA, i);
		}
		public TerminalNode PARANTEZA_D() { return getToken(gramaticaParser.PARANTEZA_D, 0); }
		public TerminalNode PARANTEZA_I() { return getToken(gramaticaParser.PARANTEZA_I, 0); }
		public TerminalNode DOUA_PUNCTE() { return getToken(gramaticaParser.DOUA_PUNCTE, 0); }
		public List<TerminalNode> TIP_DATA() { return getTokens(gramaticaParser.TIP_DATA); }
		public TerminalNode TIP_DATA(int i) {
			return getToken(gramaticaParser.TIP_DATA, i);
		}
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public TerminalNode DIEZ() { return getToken(gramaticaParser.DIEZ, 0); }
		public TerminalNode AMPERSAND() { return getToken(gramaticaParser.AMPERSAND, 0); }
		public DeclarareFunctieContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarareFunctie; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterDeclarareFunctie(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitDeclarareFunctie(this);
		}
	}

	public final DeclarareFunctieContext declarareFunctie() throws RecognitionException {
		DeclarareFunctieContext _localctx = new DeclarareFunctieContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_declarareFunctie);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			match(FUNCTIE);
			setState(57);
			match(VARIABILA);
			setState(58);
			match(PARANTEZA_D);
			setState(62);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TIP_DATA) {
				{
				setState(59);
				match(TIP_DATA);
				setState(60);
				_la = _input.LA(1);
				if ( !(_la==AMPERSAND || _la==DIEZ) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(61);
				match(VARIABILA);
				}
			}

			setState(64);
			match(PARANTEZA_I);
			setState(65);
			match(DOUA_PUNCTE);
			setState(66);
			match(TIP_DATA);
			setState(67);
			bloc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ApelareFunctieContext extends ParserRuleContext {
		public TerminalNode VARIABILA() { return getToken(gramaticaParser.VARIABILA, 0); }
		public TerminalNode PARANTEZA_D() { return getToken(gramaticaParser.PARANTEZA_D, 0); }
		public TerminalNode PARANTEZA_I() { return getToken(gramaticaParser.PARANTEZA_I, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ApelareFunctieContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_apelareFunctie; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterApelareFunctie(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitApelareFunctie(this);
		}
	}

	public final ApelareFunctieContext apelareFunctie() throws RecognitionException {
		ApelareFunctieContext _localctx = new ApelareFunctieContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_apelareFunctie);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			match(VARIABILA);
			setState(70);
			match(PARANTEZA_D);
			setState(72);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTREG) | (1L << REAL) | (1L << NU) | (1L << MINUS) | (1L << PARANTEZA_D) | (1L << VARIABILA) | (1L << CARACTER))) != 0)) {
				{
				setState(71);
				expr(0);
				}
			}

			setState(74);
			match(PARANTEZA_I);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParsareContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(gramaticaParser.EOF, 0); }
		public List<LinieContext> linie() {
			return getRuleContexts(LinieContext.class);
		}
		public LinieContext linie(int i) {
			return getRuleContext(LinieContext.class,i);
		}
		public ParsareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parsare; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterParsare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitParsare(this);
		}
	}

	public final ParsareContext parsare() throws RecognitionException {
		ParsareContext _localctx = new ParsareContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_parsare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RETURN) | (1L << INCREMENTARE) | (1L << DECREMENTARE) | (1L << ACOLADA_D) | (1L << DACA) | (1L << PENTRU) | (1L << CAT_TIMP) | (1L << EXECUTA) | (1L << SELECTARE) | (1L << FUNCTIE) | (1L << TIP_DATA) | (1L << VARIABILA))) != 0)) {
				{
				{
				setState(76);
				linie();
				}
				}
				setState(81);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(82);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlocContext extends ParserRuleContext {
		public TerminalNode ACOLADA_D() { return getToken(gramaticaParser.ACOLADA_D, 0); }
		public TerminalNode ACOLADA_I() { return getToken(gramaticaParser.ACOLADA_I, 0); }
		public List<LinieContext> linie() {
			return getRuleContexts(LinieContext.class);
		}
		public LinieContext linie(int i) {
			return getRuleContext(LinieContext.class,i);
		}
		public BlocContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterBloc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitBloc(this);
		}
	}

	public final BlocContext bloc() throws RecognitionException {
		BlocContext _localctx = new BlocContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_bloc);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			match(ACOLADA_D);
			setState(88);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RETURN) | (1L << INCREMENTARE) | (1L << DECREMENTARE) | (1L << ACOLADA_D) | (1L << DACA) | (1L << PENTRU) | (1L << CAT_TIMP) | (1L << EXECUTA) | (1L << SELECTARE) | (1L << FUNCTIE) | (1L << TIP_DATA) | (1L << VARIABILA))) != 0)) {
				{
				{
				setState(85);
				linie();
				}
				}
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(91);
			match(ACOLADA_I);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_lineContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(gramaticaParser.RETURN, 0); }
		public TerminalNode PUNCT_VIRGULA() { return getToken(gramaticaParser.PUNCT_VIRGULA, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Return_lineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_line; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterReturn_line(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitReturn_line(this);
		}
	}

	public final Return_lineContext return_line() throws RecognitionException {
		Return_lineContext _localctx = new Return_lineContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_return_line);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93);
			match(RETURN);
			setState(95);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTREG) | (1L << REAL) | (1L << NU) | (1L << MINUS) | (1L << PARANTEZA_D) | (1L << VARIABILA) | (1L << CARACTER))) != 0)) {
				{
				setState(94);
				expr(0);
				}
			}

			setState(97);
			match(PUNCT_VIRGULA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LinieContext extends ParserRuleContext {
		public DeclarareVariabilaContext declarareVariabila() {
			return getRuleContext(DeclarareVariabilaContext.class,0);
		}
		public TerminalNode PUNCT_VIRGULA() { return getToken(gramaticaParser.PUNCT_VIRGULA, 0); }
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public DeclarareFunctieContext declarareFunctie() {
			return getRuleContext(DeclarareFunctieContext.class,0);
		}
		public AtribuireContext atribuire() {
			return getRuleContext(AtribuireContext.class,0);
		}
		public Incrementari_decrementariContext incrementari_decrementari() {
			return getRuleContext(Incrementari_decrementariContext.class,0);
		}
		public Daca_parametriiContext daca_parametrii() {
			return getRuleContext(Daca_parametriiContext.class,0);
		}
		public Executa_pana_cand_parametriContext executa_pana_cand_parametri() {
			return getRuleContext(Executa_pana_cand_parametriContext.class,0);
		}
		public Cat_timp_parametriiContext cat_timp_parametrii() {
			return getRuleContext(Cat_timp_parametriiContext.class,0);
		}
		public Pentru_parametriContext pentru_parametri() {
			return getRuleContext(Pentru_parametriContext.class,0);
		}
		public ApelareFunctieContext apelareFunctie() {
			return getRuleContext(ApelareFunctieContext.class,0);
		}
		public Selectare_parametriContext selectare_parametri() {
			return getRuleContext(Selectare_parametriContext.class,0);
		}
		public Return_lineContext return_line() {
			return getRuleContext(Return_lineContext.class,0);
		}
		public LinieContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_linie; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterLinie(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitLinie(this);
		}
	}

	public final LinieContext linie() throws RecognitionException {
		LinieContext _localctx = new LinieContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_linie);
		try {
			setState(119);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(99);
				declarareVariabila();
				setState(100);
				match(PUNCT_VIRGULA);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(102);
				bloc();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(103);
				declarareFunctie();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(104);
				atribuire();
				setState(105);
				match(PUNCT_VIRGULA);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(107);
				incrementari_decrementari();
				setState(108);
				match(PUNCT_VIRGULA);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(110);
				daca_parametrii();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(111);
				executa_pana_cand_parametri();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(112);
				cat_timp_parametrii();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(113);
				pentru_parametri();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(114);
				apelareFunctie();
				setState(115);
				match(PUNCT_VIRGULA);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(117);
				selectare_parametri();
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(118);
				return_line();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtribuireContext extends ParserRuleContext {
		public FolosireVariabilaContext folosireVariabila() {
			return getRuleContext(FolosireVariabilaContext.class,0);
		}
		public TerminalNode ATRIBUIRE() { return getToken(gramaticaParser.ATRIBUIRE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode PLUS_ATR() { return getToken(gramaticaParser.PLUS_ATR, 0); }
		public TerminalNode MINUS_ATR() { return getToken(gramaticaParser.MINUS_ATR, 0); }
		public TerminalNode IMPARTIRE_ATR() { return getToken(gramaticaParser.IMPARTIRE_ATR, 0); }
		public TerminalNode INMULTIRE_ATR() { return getToken(gramaticaParser.INMULTIRE_ATR, 0); }
		public AtribuireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atribuire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterAtribuire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitAtribuire(this);
		}
	}

	public final AtribuireContext atribuire() throws RecognitionException {
		AtribuireContext _localctx = new AtribuireContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_atribuire);
		try {
			setState(141);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(121);
				folosireVariabila();
				setState(122);
				match(ATRIBUIRE);
				setState(123);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(125);
				folosireVariabila();
				setState(126);
				match(PLUS_ATR);
				setState(127);
				expr(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(129);
				folosireVariabila();
				setState(130);
				match(MINUS_ATR);
				setState(131);
				expr(0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(133);
				folosireVariabila();
				setState(134);
				match(IMPARTIRE_ATR);
				setState(135);
				expr(0);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(137);
				folosireVariabila();
				setState(138);
				match(INMULTIRE_ATR);
				setState(139);
				expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Daca_parametriiContext extends ParserRuleContext {
		public List<TerminalNode> DACA() { return getTokens(gramaticaParser.DACA); }
		public TerminalNode DACA(int i) {
			return getToken(gramaticaParser.DACA, i);
		}
		public List<Bloc_conditieContext> bloc_conditie() {
			return getRuleContexts(Bloc_conditieContext.class);
		}
		public Bloc_conditieContext bloc_conditie(int i) {
			return getRuleContext(Bloc_conditieContext.class,i);
		}
		public List<TerminalNode> ALTFEL() { return getTokens(gramaticaParser.ALTFEL); }
		public TerminalNode ALTFEL(int i) {
			return getToken(gramaticaParser.ALTFEL, i);
		}
		public Bloc_declaratiiContext bloc_declaratii() {
			return getRuleContext(Bloc_declaratiiContext.class,0);
		}
		public Daca_parametriiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_daca_parametrii; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterDaca_parametrii(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitDaca_parametrii(this);
		}
	}

	public final Daca_parametriiContext daca_parametrii() throws RecognitionException {
		Daca_parametriiContext _localctx = new Daca_parametriiContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_daca_parametrii);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(143);
			match(DACA);
			setState(144);
			bloc_conditie();
			setState(150);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(145);
					match(ALTFEL);
					setState(146);
					match(DACA);
					setState(147);
					bloc_conditie();
					}
					} 
				}
				setState(152);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			setState(155);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(153);
				match(ALTFEL);
				setState(154);
				bloc_declaratii();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bloc_conditieContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Bloc_declaratiiContext bloc_declaratii() {
			return getRuleContext(Bloc_declaratiiContext.class,0);
		}
		public Bloc_conditieContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloc_conditie; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterBloc_conditie(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitBloc_conditie(this);
		}
	}

	public final Bloc_conditieContext bloc_conditie() throws RecognitionException {
		Bloc_conditieContext _localctx = new Bloc_conditieContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_bloc_conditie);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			expr(0);
			setState(158);
			bloc_declaratii();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bloc_declaratiiContext extends ParserRuleContext {
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public LinieContext linie() {
			return getRuleContext(LinieContext.class,0);
		}
		public Bloc_declaratiiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloc_declaratii; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterBloc_declaratii(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitBloc_declaratii(this);
		}
	}

	public final Bloc_declaratiiContext bloc_declaratii() throws RecognitionException {
		Bloc_declaratiiContext _localctx = new Bloc_declaratiiContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_bloc_declaratii);
		try {
			setState(162);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(160);
				bloc();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(161);
				linie();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cat_timp_parametriiContext extends ParserRuleContext {
		public TerminalNode CAT_TIMP() { return getToken(gramaticaParser.CAT_TIMP, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public Cat_timp_parametriiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cat_timp_parametrii; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterCat_timp_parametrii(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitCat_timp_parametrii(this);
		}
	}

	public final Cat_timp_parametriiContext cat_timp_parametrii() throws RecognitionException {
		Cat_timp_parametriiContext _localctx = new Cat_timp_parametriiContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_cat_timp_parametrii);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			match(CAT_TIMP);
			setState(165);
			expr(0);
			setState(166);
			bloc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pentru_parametriContext extends ParserRuleContext {
		public TerminalNode PENTRU() { return getToken(gramaticaParser.PENTRU, 0); }
		public TerminalNode PARANTEZA_D() { return getToken(gramaticaParser.PARANTEZA_D, 0); }
		public List<TerminalNode> PUNCT_VIRGULA() { return getTokens(gramaticaParser.PUNCT_VIRGULA); }
		public TerminalNode PUNCT_VIRGULA(int i) {
			return getToken(gramaticaParser.PUNCT_VIRGULA, i);
		}
		public TerminalNode PARANTEZA_I() { return getToken(gramaticaParser.PARANTEZA_I, 0); }
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public List<Pentru_initializariContext> pentru_initializari() {
			return getRuleContexts(Pentru_initializariContext.class);
		}
		public Pentru_initializariContext pentru_initializari(int i) {
			return getRuleContext(Pentru_initializariContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<Pentru_operatiiContext> pentru_operatii() {
			return getRuleContexts(Pentru_operatiiContext.class);
		}
		public Pentru_operatiiContext pentru_operatii(int i) {
			return getRuleContext(Pentru_operatiiContext.class,i);
		}
		public Pentru_parametriContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pentru_parametri; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterPentru_parametri(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitPentru_parametri(this);
		}
	}

	public final Pentru_parametriContext pentru_parametri() throws RecognitionException {
		Pentru_parametriContext _localctx = new Pentru_parametriContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_pentru_parametri);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(168);
			match(PENTRU);
			setState(169);
			match(PARANTEZA_D);
			setState(173);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TIP_DATA || _la==VARIABILA) {
				{
				{
				setState(170);
				pentru_initializari();
				}
				}
				setState(175);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(176);
			match(PUNCT_VIRGULA);
			setState(180);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTREG) | (1L << REAL) | (1L << NU) | (1L << MINUS) | (1L << PARANTEZA_D) | (1L << VARIABILA) | (1L << CARACTER))) != 0)) {
				{
				{
				setState(177);
				expr(0);
				}
				}
				setState(182);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(183);
			match(PUNCT_VIRGULA);
			setState(187);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INCREMENTARE) | (1L << DECREMENTARE) | (1L << VARIABILA))) != 0)) {
				{
				{
				setState(184);
				pentru_operatii();
				}
				}
				setState(189);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(190);
			match(PARANTEZA_I);
			setState(191);
			bloc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pentru_initializariContext extends ParserRuleContext {
		public AtribuireContext atribuire() {
			return getRuleContext(AtribuireContext.class,0);
		}
		public DeclarareVariabilaContext declarareVariabila() {
			return getRuleContext(DeclarareVariabilaContext.class,0);
		}
		public TerminalNode VIRGULA() { return getToken(gramaticaParser.VIRGULA, 0); }
		public Pentru_initializariContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pentru_initializari; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterPentru_initializari(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitPentru_initializari(this);
		}
	}

	public final Pentru_initializariContext pentru_initializari() throws RecognitionException {
		Pentru_initializariContext _localctx = new Pentru_initializariContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_pentru_initializari);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VARIABILA:
				{
				setState(193);
				atribuire();
				}
				break;
			case TIP_DATA:
				{
				setState(194);
				declarareVariabila();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(198);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VIRGULA) {
				{
				setState(197);
				match(VIRGULA);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pentru_operatiiContext extends ParserRuleContext {
		public Incrementari_decrementariContext incrementari_decrementari() {
			return getRuleContext(Incrementari_decrementariContext.class,0);
		}
		public AtribuireContext atribuire() {
			return getRuleContext(AtribuireContext.class,0);
		}
		public TerminalNode VIRGULA() { return getToken(gramaticaParser.VIRGULA, 0); }
		public Pentru_operatiiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pentru_operatii; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterPentru_operatii(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitPentru_operatii(this);
		}
	}

	public final Pentru_operatiiContext pentru_operatii() throws RecognitionException {
		Pentru_operatiiContext _localctx = new Pentru_operatiiContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_pentru_operatii);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(202);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				setState(200);
				incrementari_decrementari();
				}
				break;
			case 2:
				{
				setState(201);
				atribuire();
				}
				break;
			}
			setState(205);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VIRGULA) {
				{
				setState(204);
				match(VIRGULA);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Incrementari_decrementariContext extends ParserRuleContext {
		public FolosireVariabilaContext folosireVariabila() {
			return getRuleContext(FolosireVariabilaContext.class,0);
		}
		public TerminalNode INCREMENTARE() { return getToken(gramaticaParser.INCREMENTARE, 0); }
		public TerminalNode DECREMENTARE() { return getToken(gramaticaParser.DECREMENTARE, 0); }
		public Incrementari_decrementariContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_incrementari_decrementari; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterIncrementari_decrementari(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitIncrementari_decrementari(this);
		}
	}

	public final Incrementari_decrementariContext incrementari_decrementari() throws RecognitionException {
		Incrementari_decrementariContext _localctx = new Incrementari_decrementariContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_incrementari_decrementari);
		try {
			setState(217);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(207);
				folosireVariabila();
				setState(208);
				match(INCREMENTARE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(210);
				folosireVariabila();
				setState(211);
				match(DECREMENTARE);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(213);
				match(INCREMENTARE);
				setState(214);
				folosireVariabila();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(215);
				match(DECREMENTARE);
				setState(216);
				folosireVariabila();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Selectare_parametriContext extends ParserRuleContext {
		public TerminalNode SELECTARE() { return getToken(gramaticaParser.SELECTARE, 0); }
		public FolosireVariabilaContext folosireVariabila() {
			return getRuleContext(FolosireVariabilaContext.class,0);
		}
		public TerminalNode ACOLADA_D() { return getToken(gramaticaParser.ACOLADA_D, 0); }
		public TerminalNode ACOLADA_I() { return getToken(gramaticaParser.ACOLADA_I, 0); }
		public List<Bloc_caz_selectareContext> bloc_caz_selectare() {
			return getRuleContexts(Bloc_caz_selectareContext.class);
		}
		public Bloc_caz_selectareContext bloc_caz_selectare(int i) {
			return getRuleContext(Bloc_caz_selectareContext.class,i);
		}
		public Caz_implicit_selectareContext caz_implicit_selectare() {
			return getRuleContext(Caz_implicit_selectareContext.class,0);
		}
		public Selectare_parametriContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectare_parametri; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterSelectare_parametri(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitSelectare_parametri(this);
		}
	}

	public final Selectare_parametriContext selectare_parametri() throws RecognitionException {
		Selectare_parametriContext _localctx = new Selectare_parametriContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_selectare_parametri);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(219);
			match(SELECTARE);
			setState(220);
			folosireVariabila();
			setState(221);
			match(ACOLADA_D);
			setState(225);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==CAZ) {
				{
				{
				setState(222);
				bloc_caz_selectare();
				}
				}
				setState(227);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(229);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IMPLICIT) {
				{
				setState(228);
				caz_implicit_selectare();
				}
			}

			setState(231);
			match(ACOLADA_I);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bloc_caz_selectareContext extends ParserRuleContext {
		public TerminalNode CAZ() { return getToken(gramaticaParser.CAZ, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode DOUA_PUNCTE() { return getToken(gramaticaParser.DOUA_PUNCTE, 0); }
		public List<LinieContext> linie() {
			return getRuleContexts(LinieContext.class);
		}
		public LinieContext linie(int i) {
			return getRuleContext(LinieContext.class,i);
		}
		public TerminalNode INTRERUPE() { return getToken(gramaticaParser.INTRERUPE, 0); }
		public TerminalNode PUNCT_VIRGULA() { return getToken(gramaticaParser.PUNCT_VIRGULA, 0); }
		public Bloc_caz_selectareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloc_caz_selectare; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterBloc_caz_selectare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitBloc_caz_selectare(this);
		}
	}

	public final Bloc_caz_selectareContext bloc_caz_selectare() throws RecognitionException {
		Bloc_caz_selectareContext _localctx = new Bloc_caz_selectareContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_bloc_caz_selectare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(233);
			match(CAZ);
			setState(234);
			expr(0);
			setState(235);
			match(DOUA_PUNCTE);
			setState(239);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RETURN) | (1L << INCREMENTARE) | (1L << DECREMENTARE) | (1L << ACOLADA_D) | (1L << DACA) | (1L << PENTRU) | (1L << CAT_TIMP) | (1L << EXECUTA) | (1L << SELECTARE) | (1L << FUNCTIE) | (1L << TIP_DATA) | (1L << VARIABILA))) != 0)) {
				{
				{
				setState(236);
				linie();
				}
				}
				setState(241);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INTRERUPE) {
				{
				setState(242);
				match(INTRERUPE);
				setState(243);
				match(PUNCT_VIRGULA);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Caz_implicit_selectareContext extends ParserRuleContext {
		public TerminalNode IMPLICIT() { return getToken(gramaticaParser.IMPLICIT, 0); }
		public TerminalNode DOUA_PUNCTE() { return getToken(gramaticaParser.DOUA_PUNCTE, 0); }
		public List<LinieContext> linie() {
			return getRuleContexts(LinieContext.class);
		}
		public LinieContext linie(int i) {
			return getRuleContext(LinieContext.class,i);
		}
		public TerminalNode INTRERUPE() { return getToken(gramaticaParser.INTRERUPE, 0); }
		public TerminalNode PUNCT_VIRGULA() { return getToken(gramaticaParser.PUNCT_VIRGULA, 0); }
		public Caz_implicit_selectareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caz_implicit_selectare; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterCaz_implicit_selectare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitCaz_implicit_selectare(this);
		}
	}

	public final Caz_implicit_selectareContext caz_implicit_selectare() throws RecognitionException {
		Caz_implicit_selectareContext _localctx = new Caz_implicit_selectareContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_caz_implicit_selectare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(246);
			match(IMPLICIT);
			setState(247);
			match(DOUA_PUNCTE);
			setState(251);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RETURN) | (1L << INCREMENTARE) | (1L << DECREMENTARE) | (1L << ACOLADA_D) | (1L << DACA) | (1L << PENTRU) | (1L << CAT_TIMP) | (1L << EXECUTA) | (1L << SELECTARE) | (1L << FUNCTIE) | (1L << TIP_DATA) | (1L << VARIABILA))) != 0)) {
				{
				{
				setState(248);
				linie();
				}
				}
				setState(253);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(256);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INTRERUPE) {
				{
				setState(254);
				match(INTRERUPE);
				setState(255);
				match(PUNCT_VIRGULA);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Executa_pana_cand_parametriContext extends ParserRuleContext {
		public TerminalNode EXECUTA() { return getToken(gramaticaParser.EXECUTA, 0); }
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public TerminalNode PANA_CAND() { return getToken(gramaticaParser.PANA_CAND, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode PUNCT_VIRGULA() { return getToken(gramaticaParser.PUNCT_VIRGULA, 0); }
		public Executa_pana_cand_parametriContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_executa_pana_cand_parametri; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterExecuta_pana_cand_parametri(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitExecuta_pana_cand_parametri(this);
		}
	}

	public final Executa_pana_cand_parametriContext executa_pana_cand_parametri() throws RecognitionException {
		Executa_pana_cand_parametriContext _localctx = new Executa_pana_cand_parametriContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_executa_pana_cand_parametri);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			match(EXECUTA);
			setState(259);
			bloc();
			setState(260);
			match(PANA_CAND);
			setState(261);
			expr(0);
			setState(262);
			match(PUNCT_VIRGULA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Token op;
		public TerminalNode MINUS() { return getToken(gramaticaParser.MINUS, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode NU() { return getToken(gramaticaParser.NU, 0); }
		public TerminalNode PARANTEZA_D() { return getToken(gramaticaParser.PARANTEZA_D, 0); }
		public TerminalNode PARANTEZA_I() { return getToken(gramaticaParser.PARANTEZA_I, 0); }
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public TerminalNode INMULTIRE() { return getToken(gramaticaParser.INMULTIRE, 0); }
		public TerminalNode IMPARTIRE() { return getToken(gramaticaParser.IMPARTIRE, 0); }
		public TerminalNode PROCENT() { return getToken(gramaticaParser.PROCENT, 0); }
		public TerminalNode PLUS() { return getToken(gramaticaParser.PLUS, 0); }
		public TerminalNode MIC_EGAL() { return getToken(gramaticaParser.MIC_EGAL, 0); }
		public TerminalNode MARE_EGAL() { return getToken(gramaticaParser.MARE_EGAL, 0); }
		public TerminalNode MIC() { return getToken(gramaticaParser.MIC, 0); }
		public TerminalNode MARE() { return getToken(gramaticaParser.MARE, 0); }
		public TerminalNode EGAL() { return getToken(gramaticaParser.EGAL, 0); }
		public TerminalNode DIFERIT() { return getToken(gramaticaParser.DIFERIT, 0); }
		public TerminalNode SI() { return getToken(gramaticaParser.SI, 0); }
		public TerminalNode SAU() { return getToken(gramaticaParser.SAU, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(274);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MINUS:
				{
				setState(265);
				match(MINUS);
				setState(266);
				expr(10);
				}
				break;
			case NU:
				{
				setState(267);
				match(NU);
				setState(268);
				expr(9);
				}
				break;
			case PARANTEZA_D:
				{
				setState(269);
				match(PARANTEZA_D);
				setState(270);
				expr(0);
				setState(271);
				match(PARANTEZA_I);
				}
				break;
			case INTREG:
			case REAL:
			case VARIABILA:
			case CARACTER:
				{
				setState(273);
				atom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(296);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(294);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(276);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(277);
						((ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INMULTIRE) | (1L << IMPARTIRE) | (1L << PROCENT))) != 0)) ) {
							((ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(278);
						expr(8);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(279);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(280);
						((ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
							((ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(281);
						expr(7);
						}
						break;
					case 3:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(282);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(283);
						((ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MARE) | (1L << MIC) | (1L << MARE_EGAL) | (1L << MIC_EGAL))) != 0)) ) {
							((ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(284);
						expr(6);
						}
						break;
					case 4:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(285);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(286);
						((ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==EGAL || _la==DIFERIT) ) {
							((ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(287);
						expr(5);
						}
						break;
					case 5:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(288);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(289);
						match(SI);
						setState(290);
						expr(4);
						}
						break;
					case 6:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(291);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(292);
						match(SAU);
						setState(293);
						expr(3);
						}
						break;
					}
					} 
				}
				setState(298);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public TerminalNode INTREG() { return getToken(gramaticaParser.INTREG, 0); }
		public TerminalNode REAL() { return getToken(gramaticaParser.REAL, 0); }
		public FolosireVariabilaContext folosireVariabila() {
			return getRuleContext(FolosireVariabilaContext.class,0);
		}
		public ApelareFunctieContext apelareFunctie() {
			return getRuleContext(ApelareFunctieContext.class,0);
		}
		public TerminalNode CARACTER() { return getToken(gramaticaParser.CARACTER, 0); }
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof gramaticaListener ) ((gramaticaListener)listener).exitAtom(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_atom);
		int _la;
		try {
			setState(303);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(299);
				_la = _input.LA(1);
				if ( !(_la==INTREG || _la==REAL) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(300);
				folosireVariabila();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(301);
				apelareFunctie();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(302);
				match(CARACTER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 21:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 7);
		case 1:
			return precpred(_ctx, 6);
		case 2:
			return precpred(_ctx, 5);
		case 3:
			return precpred(_ctx, 4);
		case 4:
			return precpred(_ctx, 3);
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\67\u0134\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\3\2\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\5\39\n\3\3\4\3\4\3\4\3\4\3\4\3\4\5\4A\n\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\5\3\5\3\5\5\5K\n\5\3\5\3\5\3\6\7\6P\n\6\f\6\16\6S\13"+
		"\6\3\6\3\6\3\7\3\7\7\7Y\n\7\f\7\16\7\\\13\7\3\7\3\7\3\b\3\b\5\bb\n\b\3"+
		"\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\5\tz\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u0090\n\n\3\13\3\13\3\13\3\13"+
		"\3\13\7\13\u0097\n\13\f\13\16\13\u009a\13\13\3\13\3\13\5\13\u009e\n\13"+
		"\3\f\3\f\3\f\3\r\3\r\5\r\u00a5\n\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17"+
		"\7\17\u00ae\n\17\f\17\16\17\u00b1\13\17\3\17\3\17\7\17\u00b5\n\17\f\17"+
		"\16\17\u00b8\13\17\3\17\3\17\7\17\u00bc\n\17\f\17\16\17\u00bf\13\17\3"+
		"\17\3\17\3\17\3\20\3\20\5\20\u00c6\n\20\3\20\5\20\u00c9\n\20\3\21\3\21"+
		"\5\21\u00cd\n\21\3\21\5\21\u00d0\n\21\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\5\22\u00dc\n\22\3\23\3\23\3\23\3\23\7\23\u00e2\n\23"+
		"\f\23\16\23\u00e5\13\23\3\23\5\23\u00e8\n\23\3\23\3\23\3\24\3\24\3\24"+
		"\3\24\7\24\u00f0\n\24\f\24\16\24\u00f3\13\24\3\24\3\24\5\24\u00f7\n\24"+
		"\3\25\3\25\3\25\7\25\u00fc\n\25\f\25\16\25\u00ff\13\25\3\25\3\25\5\25"+
		"\u0103\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\5\27\u0115\n\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u0129\n\27"+
		"\f\27\16\27\u012c\13\27\3\30\3\30\3\30\3\30\5\30\u0132\n\30\3\30\2\3,"+
		"\31\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\2\b\3\2\33\34\4\2"+
		"\17\20\27\27\3\2\31\32\3\2\13\16\3\2\t\n\3\2\3\4\2\u0150\2\60\3\2\2\2"+
		"\48\3\2\2\2\6:\3\2\2\2\bG\3\2\2\2\nQ\3\2\2\2\fV\3\2\2\2\16_\3\2\2\2\20"+
		"y\3\2\2\2\22\u008f\3\2\2\2\24\u0091\3\2\2\2\26\u009f\3\2\2\2\30\u00a4"+
		"\3\2\2\2\32\u00a6\3\2\2\2\34\u00aa\3\2\2\2\36\u00c5\3\2\2\2 \u00cc\3\2"+
		"\2\2\"\u00db\3\2\2\2$\u00dd\3\2\2\2&\u00eb\3\2\2\2(\u00f8\3\2\2\2*\u0104"+
		"\3\2\2\2,\u0114\3\2\2\2.\u0131\3\2\2\2\60\61\7\64\2\2\61\3\3\2\2\2\62"+
		"\63\7\63\2\2\63\64\7\64\2\2\64\65\7#\2\2\659\5,\27\2\66\67\7\63\2\2\67"+
		"9\7\64\2\28\62\3\2\2\28\66\3\2\2\29\5\3\2\2\2:;\7\60\2\2;<\7\64\2\2<@"+
		"\7\35\2\2=>\7\63\2\2>?\t\2\2\2?A\7\64\2\2@=\3\2\2\2@A\3\2\2\2AB\3\2\2"+
		"\2BC\7\36\2\2CD\7!\2\2DE\7\63\2\2EF\5\f\7\2F\7\3\2\2\2GH\7\64\2\2HJ\7"+
		"\35\2\2IK\5,\27\2JI\3\2\2\2JK\3\2\2\2KL\3\2\2\2LM\7\36\2\2M\t\3\2\2\2"+
		"NP\5\20\t\2ON\3\2\2\2PS\3\2\2\2QO\3\2\2\2QR\3\2\2\2RT\3\2\2\2SQ\3\2\2"+
		"\2TU\7\2\2\3U\13\3\2\2\2VZ\7\37\2\2WY\5\20\t\2XW\3\2\2\2Y\\\3\2\2\2ZX"+
		"\3\2\2\2Z[\3\2\2\2[]\3\2\2\2\\Z\3\2\2\2]^\7 \2\2^\r\3\2\2\2_a\7\5\2\2"+
		"`b\5,\27\2a`\3\2\2\2ab\3\2\2\2bc\3\2\2\2cd\7%\2\2d\17\3\2\2\2ef\5\4\3"+
		"\2fg\7%\2\2gz\3\2\2\2hz\5\f\7\2iz\5\6\4\2jk\5\22\n\2kl\7%\2\2lz\3\2\2"+
		"\2mn\5\"\22\2no\7%\2\2oz\3\2\2\2pz\5\24\13\2qz\5*\26\2rz\5\32\16\2sz\5"+
		"\34\17\2tu\5\b\5\2uv\7%\2\2vz\3\2\2\2wz\5$\23\2xz\5\16\b\2ye\3\2\2\2y"+
		"h\3\2\2\2yi\3\2\2\2yj\3\2\2\2ym\3\2\2\2yp\3\2\2\2yq\3\2\2\2yr\3\2\2\2"+
		"ys\3\2\2\2yt\3\2\2\2yw\3\2\2\2yx\3\2\2\2z\21\3\2\2\2{|\5\2\2\2|}\7#\2"+
		"\2}~\5,\27\2~\u0090\3\2\2\2\177\u0080\5\2\2\2\u0080\u0081\7\21\2\2\u0081"+
		"\u0082\5,\27\2\u0082\u0090\3\2\2\2\u0083\u0084\5\2\2\2\u0084\u0085\7\23"+
		"\2\2\u0085\u0086\5,\27\2\u0086\u0090\3\2\2\2\u0087\u0088\5\2\2\2\u0088"+
		"\u0089\7\26\2\2\u0089\u008a\5,\27\2\u008a\u0090\3\2\2\2\u008b\u008c\5"+
		"\2\2\2\u008c\u008d\7\25\2\2\u008d\u008e\5,\27\2\u008e\u0090\3\2\2\2\u008f"+
		"{\3\2\2\2\u008f\177\3\2\2\2\u008f\u0083\3\2\2\2\u008f\u0087\3\2\2\2\u008f"+
		"\u008b\3\2\2\2\u0090\23\3\2\2\2\u0091\u0092\7&\2\2\u0092\u0098\5\26\f"+
		"\2\u0093\u0094\7\'\2\2\u0094\u0095\7&\2\2\u0095\u0097\5\26\f\2\u0096\u0093"+
		"\3\2\2\2\u0097\u009a\3\2\2\2\u0098\u0096\3\2\2\2\u0098\u0099\3\2\2\2\u0099"+
		"\u009d\3\2\2\2\u009a\u0098\3\2\2\2\u009b\u009c\7\'\2\2\u009c\u009e\5\30"+
		"\r\2\u009d\u009b\3\2\2\2\u009d\u009e\3\2\2\2\u009e\25\3\2\2\2\u009f\u00a0"+
		"\5,\27\2\u00a0\u00a1\5\30\r\2\u00a1\27\3\2\2\2\u00a2\u00a5\5\f\7\2\u00a3"+
		"\u00a5\5\20\t\2\u00a4\u00a2\3\2\2\2\u00a4\u00a3\3\2\2\2\u00a5\31\3\2\2"+
		"\2\u00a6\u00a7\7)\2\2\u00a7\u00a8\5,\27\2\u00a8\u00a9\5\f\7\2\u00a9\33"+
		"\3\2\2\2\u00aa\u00ab\7(\2\2\u00ab\u00af\7\35\2\2\u00ac\u00ae\5\36\20\2"+
		"\u00ad\u00ac\3\2\2\2\u00ae\u00b1\3\2\2\2\u00af\u00ad\3\2\2\2\u00af\u00b0"+
		"\3\2\2\2\u00b0\u00b2\3\2\2\2\u00b1\u00af\3\2\2\2\u00b2\u00b6\7%\2\2\u00b3"+
		"\u00b5\5,\27\2\u00b4\u00b3\3\2\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b4\3\2"+
		"\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b9\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b9"+
		"\u00bd\7%\2\2\u00ba\u00bc\5 \21\2\u00bb\u00ba\3\2\2\2\u00bc\u00bf\3\2"+
		"\2\2\u00bd\u00bb\3\2\2\2\u00bd\u00be\3\2\2\2\u00be\u00c0\3\2\2\2\u00bf"+
		"\u00bd\3\2\2\2\u00c0\u00c1\7\36\2\2\u00c1\u00c2\5\f\7\2\u00c2\35\3\2\2"+
		"\2\u00c3\u00c6\5\22\n\2\u00c4\u00c6\5\4\3\2\u00c5\u00c3\3\2\2\2\u00c5"+
		"\u00c4\3\2\2\2\u00c6\u00c8\3\2\2\2\u00c7\u00c9\7$\2\2\u00c8\u00c7\3\2"+
		"\2\2\u00c8\u00c9\3\2\2\2\u00c9\37\3\2\2\2\u00ca\u00cd\5\"\22\2\u00cb\u00cd"+
		"\5\22\n\2\u00cc\u00ca\3\2\2\2\u00cc\u00cb\3\2\2\2\u00cd\u00cf\3\2\2\2"+
		"\u00ce\u00d0\7$\2\2\u00cf\u00ce\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0!\3\2"+
		"\2\2\u00d1\u00d2\5\2\2\2\u00d2\u00d3\7\22\2\2\u00d3\u00dc\3\2\2\2\u00d4"+
		"\u00d5\5\2\2\2\u00d5\u00d6\7\24\2\2\u00d6\u00dc\3\2\2\2\u00d7\u00d8\7"+
		"\22\2\2\u00d8\u00dc\5\2\2\2\u00d9\u00da\7\24\2\2\u00da\u00dc\5\2\2\2\u00db"+
		"\u00d1\3\2\2\2\u00db\u00d4\3\2\2\2\u00db\u00d7\3\2\2\2\u00db\u00d9\3\2"+
		"\2\2\u00dc#\3\2\2\2\u00dd\u00de\7,\2\2\u00de\u00df\5\2\2\2\u00df\u00e3"+
		"\7\37\2\2\u00e0\u00e2\5&\24\2\u00e1\u00e0\3\2\2\2\u00e2\u00e5\3\2\2\2"+
		"\u00e3\u00e1\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00e7\3\2\2\2\u00e5\u00e3"+
		"\3\2\2\2\u00e6\u00e8\5(\25\2\u00e7\u00e6\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8"+
		"\u00e9\3\2\2\2\u00e9\u00ea\7 \2\2\u00ea%\3\2\2\2\u00eb\u00ec\7-\2\2\u00ec"+
		"\u00ed\5,\27\2\u00ed\u00f1\7!\2\2\u00ee\u00f0\5\20\t\2\u00ef\u00ee\3\2"+
		"\2\2\u00f0\u00f3\3\2\2\2\u00f1\u00ef\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2"+
		"\u00f6\3\2\2\2\u00f3\u00f1\3\2\2\2\u00f4\u00f5\7/\2\2\u00f5\u00f7\7%\2"+
		"\2\u00f6\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\'\3\2\2\2\u00f8\u00f9"+
		"\7.\2\2\u00f9\u00fd\7!\2\2\u00fa\u00fc\5\20\t\2\u00fb\u00fa\3\2\2\2\u00fc"+
		"\u00ff\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fd\u00fe\3\2\2\2\u00fe\u0102\3\2"+
		"\2\2\u00ff\u00fd\3\2\2\2\u0100\u0101\7/\2\2\u0101\u0103\7%\2\2\u0102\u0100"+
		"\3\2\2\2\u0102\u0103\3\2\2\2\u0103)\3\2\2\2\u0104\u0105\7*\2\2\u0105\u0106"+
		"\5\f\7\2\u0106\u0107\7+\2\2\u0107\u0108\5,\27\2\u0108\u0109\7%\2\2\u0109"+
		"+\3\2\2\2\u010a\u010b\b\27\1\2\u010b\u010c\7\32\2\2\u010c\u0115\5,\27"+
		"\f\u010d\u010e\7\b\2\2\u010e\u0115\5,\27\13\u010f\u0110\7\35\2\2\u0110"+
		"\u0111\5,\27\2\u0111\u0112\7\36\2\2\u0112\u0115\3\2\2\2\u0113\u0115\5"+
		".\30\2\u0114\u010a\3\2\2\2\u0114\u010d\3\2\2\2\u0114\u010f\3\2\2\2\u0114"+
		"\u0113\3\2\2\2\u0115\u012a\3\2\2\2\u0116\u0117\f\t\2\2\u0117\u0118\t\3"+
		"\2\2\u0118\u0129\5,\27\n\u0119\u011a\f\b\2\2\u011a\u011b\t\4\2\2\u011b"+
		"\u0129\5,\27\t\u011c\u011d\f\7\2\2\u011d\u011e\t\5\2\2\u011e\u0129\5,"+
		"\27\b\u011f\u0120\f\6\2\2\u0120\u0121\t\6\2\2\u0121\u0129\5,\27\7\u0122"+
		"\u0123\f\5\2\2\u0123\u0124\7\7\2\2\u0124\u0129\5,\27\6\u0125\u0126\f\4"+
		"\2\2\u0126\u0127\7\6\2\2\u0127\u0129\5,\27\5\u0128\u0116\3\2\2\2\u0128"+
		"\u0119\3\2\2\2\u0128\u011c\3\2\2\2\u0128\u011f\3\2\2\2\u0128\u0122\3\2"+
		"\2\2\u0128\u0125\3\2\2\2\u0129\u012c\3\2\2\2\u012a\u0128\3\2\2\2\u012a"+
		"\u012b\3\2\2\2\u012b-\3\2\2\2\u012c\u012a\3\2\2\2\u012d\u0132\t\7\2\2"+
		"\u012e\u0132\5\2\2\2\u012f\u0132\5\b\5\2\u0130\u0132\7\65\2\2\u0131\u012d"+
		"\3\2\2\2\u0131\u012e\3\2\2\2\u0131\u012f\3\2\2\2\u0131\u0130\3\2\2\2\u0132"+
		"/\3\2\2\2\378@JQZay\u008f\u0098\u009d\u00a4\u00af\u00b6\u00bd\u00c5\u00c8"+
		"\u00cc\u00cf\u00db\u00e3\u00e7\u00f1\u00f6\u00fd\u0102\u0114\u0128\u012a"+
		"\u0131";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}