import java.io.File;
import java.io.IOException;

import static org.antlr.v4.runtime.CharStreams.fromFileName;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class Main {
	public static void main(String[] args) {

	    CharStream cs;
		try {
			cs = fromFileName("test.ro");
		    gramaticaLexer lexer = new gramaticaLexer(cs);  //instantiate a lexer
		    CommonTokenStream tokens = new CommonTokenStream(lexer); //scan stream for tokens
		    gramaticaParser parser = new gramaticaParser(tokens);  //parse the tokens

		    ParseTree tree = parser.parsare(); // parse the content and get the tree
		    Program program= new Program();
		    ParseTreeWalker walker = new ParseTreeWalker();
		    walker.walk(program,tree);
		    if(program.errorEncountered()) {
		    	File myObj = new File("fisier.MIPS"); 
		        if (myObj.delete()) { 
		          System.out.println("Deleted the file: " + myObj.getName());
		        } else {
		          System.out.println("Failed to delete the file.");
		        } 
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
