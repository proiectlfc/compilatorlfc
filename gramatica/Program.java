
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

public class Program implements gramaticaListener {
	FileWriter fileWriter;
	int counter = 0;
	boolean error = false;
	int counterLoop = 0;
	int counterElse = 0;
	int counterEndIf = 0;
	int counterSwitch = 0;
	int counterCase = 0;
	Stack<Integer> stackIf = new Stack<>();
	Stack<Integer> stackLoop = new Stack<>();
	Stack<Integer> stackElse = new Stack<>();
	Stack<Integer> stackSwitch = new Stack<>();
	int counterTemporar = 0;
	HashMap<String, Variabila> variabile = new HashMap<>();
	HashMap<String, FuncParams<String, Boolean>> functii = new HashMap<>();

	public Boolean errorEncountered() {
		return error;
	}

	public String getTipAtom(gramaticaParser.AtomContext ctx) {
		if (ctx.INTREG() != null) {
			return "intreg";
		} else if (ctx.REAL() != null) {
			return "real";
		} else if (ctx.CARACTER() != null) {
			return "caracter";
		} else if (ctx.folosireVariabila() != null) {
			if (variabile.get(ctx.folosireVariabila().VARIABILA().toString()) != null) {
				return variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getTip();
			}
		} else if (ctx.apelareFunctie() != null) {
			return functii.get(ctx.apelareFunctie().VARIABILA().toString()).funcType;
		}
		return "invalid";
	}

	public void printMipsCommand(String comanda, int firstParameter, String secondParameter, String thirdParameter) {
		try {
			fileWriter.write(String.format("\t%s %s, %s, %s\n", comanda, getMipsVariable(firstParameter),
					secondParameter, thirdParameter));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void printMipsCommand(String comanda, String firstParameter, String secondParameter, String thirdParameter) {
		try {
			fileWriter.write(
					String.format("\t%s %s, %s, %s\n", comanda, firstParameter, secondParameter, thirdParameter));
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void printMipsCommand(String comanda, String firstParameter, String secondParameter) {
		try {
			fileWriter.write(String.format("\t%s %s, %s\n", comanda, firstParameter, secondParameter));
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void printMipsCommand(String comanda, String firstParameter) {
		try {
			fileWriter.write(String.format("\t%s %s\n", comanda, firstParameter));
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void printMipsDeclarare(gramaticaParser.ExprContext ctx, Variabila var) {
		try {
			Double valoare = resetCounterGetValoare(ctx);
			if (valoare == null) {
				if (ctx.atom() != null) {
					if (ctx.atom().apelareFunctie() != null) {
						printMipsCommand("move", "$s0", "$v0");

					} else if (ctx.atom().folosireVariabila() != null) {
						fileWriter.write(String.format("\tsw $t%d, %d($sp)\n", counterTemporar + 1, counter));
						if (var != null) {
							counter += var.getByteNumber();
						}
					}
				} else {
					fileWriter.write("\tmove $s0, $t" + (counterTemporar + 1) + "\n");
					fileWriter.write(String.format("\tsw $s0, %d($sp)\n", counter));
					if (getTipRecursiv(ctx).equals("intreg")) {
						counter += 4;
					} else if (getTipRecursiv(ctx).equals("caracter")) {
						counter += 1;
					} else if (getTipRecursiv(ctx).equals("real")) {
						counter += 8;
					}
				}
				return;
			} else if (getTipRecursiv(ctx).equals("intreg")) {
				fileWriter.write(String.format("\tli $s0, %d\n", valoare.intValue()));
				fileWriter.write(String.format("\tsw $s0, %d($sp)\n", counter));
				counter += 4;
				if (var != null) {
					var.setByteNumber(4);
				}
			} else if (getTipRecursiv(ctx).equals("caracter")) {
				fileWriter.write(String.format("\tli $s0, %d\n", valoare.intValue()));
				fileWriter.write(String.format("\tsw $s0, %d($sp)\n", counter));
				counter += 1;
				if (var != null) {
					var.setByteNumber(1);
				}
			} else if (getTipRecursiv(ctx).equals("real")) {
				float f = valoare.floatValue();
				boolean lossless = String.valueOf(f).equals(valoare.toString());
				if (lossless) {
					fileWriter.write(String.format("\tli $s0, %s\n",
							"0x" + Integer.toHexString(Float.floatToIntBits(f)).toUpperCase()));
					fileWriter.write(String.format("\tsw $s0, %d($sp)\n", counter));
					counter += 4;
					if (var != null) {
						var.setByteNumber(4);
					}
				} else {
					fileWriter.write(String.format("\tli $s0, %s\n",
							"0x" + Long.toHexString(Double.doubleToRawLongBits(valoare))));
					fileWriter.write(String.format("\tsw $s0, %d($sp)\n", counter));
					counter += 8;
					if (var != null) {
						var.setByteNumber(8);
					}
				}
			}
		} catch (

		IOException e) {

			e.printStackTrace();
		}
	}

	public void printMipsAtribuire(String index, gramaticaParser.ExprContext ctx) {
		try {
			Double valoare = resetCounterGetValoare(ctx);
			if (valoare == null) {
				if (ctx.atom() != null) {
					if (ctx.atom().apelareFunctie() != null) {
						printMipsCommand("move", "$t" + (counterTemporar + 1), "$v0");
						fileWriter.write(String.format("\tsw $t%d, %s\n", counterTemporar + 1, index));

					} else if (ctx.atom().folosireVariabila() != null) {
						if (!index.equals("$a0")) {
							fileWriter.write(String.format("\tsw $t%d, %s\n", counterTemporar + 1, index));
						} else {
							fileWriter.write(String.format("\tmove %s, $t%d", index, counterTemporar + 1));
						}
					}
				} else {
					if (!index.equals("$a0")) {
						fileWriter.write("\tmove $s0, $t" + (counterTemporar + 1) + "\n");
						fileWriter.write(String.format("\tsw $s0, %s\n", index));
					} else {
						fileWriter.write("\tmove $a0, $t" + (counterTemporar + 1) + "\n");
					}
				}
				return;
			} else if (getTipRecursiv(ctx).equals("intreg")) {
				if (!index.equals("$a0")) {
					fileWriter.write(String.format("\tli $s0, %d\n", valoare.intValue()));
					fileWriter.write(String.format("\tsw $s0, %s\n", index));
				} else {
					fileWriter.write(String.format("\tli $s0, %d\n", valoare.intValue()));
					fileWriter.write(String.format("\tmove %s, $s0\n", index));
				}
			} else if (getTipRecursiv(ctx).equals("caracter")) {
				if (!index.equals("$a0")) {
					fileWriter.write(String.format("\tli $s0, %d\n", valoare.intValue()));
					fileWriter.write(String.format("\tsw $s0, %s\n", index));
				} else {
					fileWriter.write(String.format("\tli $s0, %d\n", valoare.intValue()));
					fileWriter.write(String.format("\tmove %s, $s0\n", index));
				}
			} else if (getTipRecursiv(ctx).equals("real")) {
				float f = valoare.floatValue();
				boolean lossless = String.valueOf(f).equals(valoare.toString());
				if (!index.equals("$a0")) {
					if (lossless) {
						fileWriter.write(String.format("\tli $s0, %s\n",
								"0x" + Integer.toHexString(Float.floatToIntBits(f)).toUpperCase()));
						fileWriter.write(String.format("\tsw $s0, %s\n", index));
					} else {
						fileWriter.write(String.format("\tli $s0, %s\n",
								"0x" + Long.toHexString(Double.doubleToRawLongBits(valoare))));
						fileWriter.write(String.format("\tsw $s0, %s\n", index));
					}
				} else {
					if (lossless) {
						fileWriter.write(String.format("\tli $s0, %s\n",
								"0x" + Integer.toHexString(Float.floatToIntBits(f)).toUpperCase()));
						fileWriter.write(String.format("\tmove %s, $s0\n", index));
					} else {
						fileWriter.write(String.format("\tli $s0, %s\n",
								"0x" + Long.toHexString(Double.doubleToRawLongBits(valoare))));
						fileWriter.write(String.format("\tmove %s, $s0\n", index));
					}
				}
			}
		} catch (

		IOException e) {

			e.printStackTrace();
		}
	}

	public double getValoareAtom(gramaticaParser.AtomContext ctx) {
		if (ctx.INTREG() != null) {
			return Integer.valueOf(ctx.INTREG().toString()) * 1.0;
		} else if (ctx.CARACTER() != null) {
			return Integer.valueOf(ctx.CARACTER().toString().charAt(1)) * 1.0;
		} else if (ctx.REAL() != null) {
			return Double.valueOf(ctx.REAL().toString());
		}
		return 0;
	}

	public String getMipsVariable(int index) {
		return String.format("$%d", index);
	}

	public Double getValoareRecursiv(gramaticaParser.ExprContext ctx) {
		boolean dreapta = false;
		if (ctx.atom() != null) {
			if (ctx.atom().INTREG() != null) {
				return Integer.valueOf(ctx.atom().INTREG().toString()) * 1.0;
			} else if (ctx.atom().CARACTER() != null) {
				return Integer.valueOf(ctx.atom().CARACTER().toString().charAt(1)) * 1.0;
			} else if (ctx.atom().REAL() != null) {
				return Double.valueOf(ctx.atom().REAL().toString());
			} else if (ctx.atom().folosireVariabila() != null) {
				++counterTemporar;
				if (!variabile.get(ctx.atom().folosireVariabila().VARIABILA().toString()).getOffset().equals("$a0")) {
					printMipsCommand("lw", "$t" + counterTemporar,
							variabile.get(ctx.atom().folosireVariabila().VARIABILA().toString()).getOffset());
				}else {
					printMipsCommand("move", "$t" + counterTemporar,
							variabile.get(ctx.atom().folosireVariabila().VARIABILA().toString()).getOffset());
				}
				return null;
			}
		}
		if (ctx.expr().size() == 1) {
			return getValoareRecursiv(ctx.expr(0));
		}
		if (ctx.expr().size() == 2) {
			Double valoare1 = null;
			Double valoare2 = null;
			String stringValoare1 = null;
			String stringValoare2 = null;
			if (ctx.expr(0).expr().size() > 0) {
				valoare1 = getValoareRecursiv(ctx.expr(0));
				valoare2 = getValoareRecursiv(ctx.expr(1));
			} else if (ctx.expr(1).expr().size() > 0) {
				valoare2 = getValoareRecursiv(ctx.expr(1));
				valoare1 = getValoareRecursiv(ctx.expr(0));
				dreapta = true;
			} else {
				valoare1 = getValoareRecursiv(ctx.expr(0));
				valoare2 = getValoareRecursiv(ctx.expr(1));
			}
			if (valoare1 != null && valoare2 != null) {
				if (ctx.PLUS() != null) {
					return valoare1 + valoare2;
				} else if (ctx.MINUS() != null) {
					return valoare1 - valoare2;
				} else if (ctx.INMULTIRE() != null) {
					return valoare1 * valoare2;
				} else if (ctx.IMPARTIRE() != null) {
					return valoare1 / valoare2;
				} else if (ctx.PROCENT() != null) {
					return valoare1 % valoare2;
				} else if (ctx.EGAL() != null) {
					return valoare1 == valoare2 ? 1.0 : 0.0;
				} else if (ctx.DIFERIT() != null) {
					return valoare1 != valoare2 ? 1.0 : 0.0;
				} else if (ctx.MARE() != null) {
					return valoare1 > valoare2 ? 1.0 : 0.0;
				} else if (ctx.MARE_EGAL() != null) {
					return valoare1 >= valoare2 ? 1.0 : 0.0;
				} else if (ctx.MIC() != null) {
					return valoare1 < valoare2 ? 1.0 : 0.0;
				} else if (ctx.MIC_EGAL() != null) {
					return valoare1 <= valoare2 ? 1.0 : 0.0;
				} else if (ctx.SI() != null) {
					return valoare1 != 0.0 && valoare2 != 0.0 ? 1.0 : 0.0;
				} else if (ctx.SAU() != null) {
					return valoare1 != 0.0 || valoare2 != 0.0 ? 1.0 : 0.0;
				}
			} else if (valoare1 == null && valoare2 == null) {
				--counterTemporar;
				if (!dreapta) {
					if (ctx.PLUS() != null) {
						printMipsCommand("add", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MINUS() != null) {
						printMipsCommand("sub", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.INMULTIRE() != null) {
						printMipsCommand("mul", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.IMPARTIRE() != null) {
						printMipsCommand("div", "$t" + counterTemporar, "$t" + (counterTemporar + 1));
						printMipsCommand("mflo", "$t" + counterTemporar);
					} else if (ctx.PROCENT() != null) {
						printMipsCommand("div", "$t" + counterTemporar, "$t" + (counterTemporar + 1));
						printMipsCommand("mfhi", "$t" + counterTemporar);
					} else if (ctx.EGAL() != null) {
						printMipsCommand("seq", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.DIFERIT() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MARE() != null) {
						printMipsCommand("sgt", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MARE_EGAL() != null) {
						printMipsCommand("sge", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MIC() != null) {
						printMipsCommand("slt", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MIC_EGAL() != null) {
						printMipsCommand("sle", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SI() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("and", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SAU() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("or", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					}
				} else {
					if (ctx.PLUS() != null) {
						printMipsCommand("add", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MINUS() != null) {
						printMipsCommand("sub", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.INMULTIRE() != null) {
						printMipsCommand("mul", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.IMPARTIRE() != null) {
						printMipsCommand("div", "$t" + (counterTemporar + 1), "$t" + counterTemporar);
						printMipsCommand("mflo", "$t" + counterTemporar);
					} else if (ctx.PROCENT() != null) {
						printMipsCommand("div", "$t" + (counterTemporar + 1), "$t" + counterTemporar);
						printMipsCommand("mfhi", "$t" + counterTemporar);
					} else if (ctx.EGAL() != null) {
						printMipsCommand("seq", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.DIFERIT() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MARE() != null) {
						printMipsCommand("sgt", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MARE_EGAL() != null) {
						printMipsCommand("sge", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MIC() != null) {
						printMipsCommand("slt", "$t" + counterTemporar, "$t" + "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MIC_EGAL() != null) {
						printMipsCommand("sle", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.SI() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("and", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SAU() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("or", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					}
				}
			} else if (valoare1 == null && valoare2 != null) {
				if (getTipRecursiv(ctx).equals("intreg")) {
					stringValoare2 = String.valueOf(valoare2.intValue());
				} else if (getTipRecursiv(ctx).equals("caracter")) {
					stringValoare2 = String.valueOf(valoare2.intValue());
				} else if (getTipRecursiv(ctx).equals("real")) {
					float f2 = valoare2.floatValue();
					boolean lossless2 = String.valueOf(f2).equals(valoare2.toString());
					if (lossless2) {
						stringValoare2 = "0x" + Integer.toHexString(Float.floatToIntBits(f2)).toUpperCase();
					} else {
						stringValoare2 = "0x" + Long.toHexString(Double.doubleToRawLongBits(valoare2));
					}
				}
				printMipsCommand("li", "$t" + (counterTemporar + 1), stringValoare2);
				if (!dreapta) {
					if (ctx.PLUS() != null) {
						printMipsCommand("add", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MINUS() != null) {
						printMipsCommand("sub", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.INMULTIRE() != null) {
						printMipsCommand("mul", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.IMPARTIRE() != null) {
						printMipsCommand("div", "$t" + counterTemporar, "$t" + (counterTemporar + 1));
						printMipsCommand("mflo", "$t" + counterTemporar);
					} else if (ctx.PROCENT() != null) {
						printMipsCommand("div", "$t" + counterTemporar, "$t" + (counterTemporar + 1));
						printMipsCommand("mfhi", "$t" + counterTemporar);
					} else if (ctx.EGAL() != null) {
						printMipsCommand("seq", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.DIFERIT() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MARE() != null) {
						printMipsCommand("sgt", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MARE_EGAL() != null) {
						printMipsCommand("sge", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MIC() != null) {
						printMipsCommand("slt", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MIC_EGAL() != null) {
						printMipsCommand("sle", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SI() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("and", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SAU() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("or", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					}
				} else {
					if (ctx.PLUS() != null) {
						printMipsCommand("add", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MINUS() != null) {
						printMipsCommand("sub", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.INMULTIRE() != null) {
						printMipsCommand("mul", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.IMPARTIRE() != null) {
						printMipsCommand("div", "$t" + (counterTemporar + 1), "$t" + counterTemporar);
						printMipsCommand("mflo", "$t" + counterTemporar);
					} else if (ctx.PROCENT() != null) {
						printMipsCommand("div", "$t" + (counterTemporar + 1), "$t" + counterTemporar);
						printMipsCommand("mfhi", "$t" + counterTemporar);
					} else if (ctx.EGAL() != null) {
						printMipsCommand("seq", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.DIFERIT() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MARE() != null) {
						printMipsCommand("sgt", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MARE_EGAL() != null) {
						printMipsCommand("sge", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MIC() != null) {
						printMipsCommand("slt", "$t" + counterTemporar, "$t" + "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MIC_EGAL() != null) {
						printMipsCommand("sle", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.SI() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("and", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SAU() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("or", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					}
				}
			} else if (valoare1 != null && valoare2 == null) {
				if (getTipRecursiv(ctx).equals("intreg")) {
					stringValoare1 = String.valueOf(valoare1.intValue());
				} else if (getTipRecursiv(ctx).equals("caracter")) {
					stringValoare1 = String.valueOf(valoare1.intValue());
				} else if (getTipRecursiv(ctx).equals("real")) {
					float f1 = valoare1.floatValue();
					boolean lossless1 = String.valueOf(f1).equals(valoare1.toString());
					if (lossless1) {
						stringValoare1 = "0x" + Integer.toHexString(Float.floatToIntBits(f1)).toUpperCase();
					} else {
						stringValoare1 = "0x" + Long.toHexString(Double.doubleToRawLongBits(valoare1));
					}
				}
				printMipsCommand("li", "$t" + (counterTemporar + 1), stringValoare1);
				if (!dreapta) {
					if (ctx.PLUS() != null) {
						printMipsCommand("add", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MINUS() != null) {
						printMipsCommand("sub", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.INMULTIRE() != null) {
						printMipsCommand("mul", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.IMPARTIRE() != null) {
						printMipsCommand("div", "$t" + counterTemporar, "$t" + (counterTemporar + 1));
						printMipsCommand("mflo", "$t" + counterTemporar);
					} else if (ctx.PROCENT() != null) {
						printMipsCommand("div", "$t" + counterTemporar, "$t" + (counterTemporar + 1));
						printMipsCommand("mfhi", "$t" + counterTemporar);
					} else if (ctx.EGAL() != null) {
						printMipsCommand("seq", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.DIFERIT() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MARE() != null) {
						printMipsCommand("sgt", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MARE_EGAL() != null) {
						printMipsCommand("sge", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MIC() != null) {
						printMipsCommand("slt", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.MIC_EGAL() != null) {
						printMipsCommand("sle", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SI() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("and", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SAU() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("or", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					}
				} else {
					if (ctx.PLUS() != null) {
						printMipsCommand("add", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MINUS() != null) {
						printMipsCommand("sub", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.INMULTIRE() != null) {
						printMipsCommand("mul", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.IMPARTIRE() != null) {
						printMipsCommand("div", "$t" + (counterTemporar + 1), "$t" + counterTemporar);
						printMipsCommand("mflo", "$t" + counterTemporar);
					} else if (ctx.PROCENT() != null) {
						printMipsCommand("div", "$t" + (counterTemporar + 1), "$t" + counterTemporar);
						printMipsCommand("mfhi", "$t" + counterTemporar);
					} else if (ctx.EGAL() != null) {
						printMipsCommand("seq", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.DIFERIT() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MARE() != null) {
						printMipsCommand("sgt", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MARE_EGAL() != null) {
						printMipsCommand("sge", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MIC() != null) {
						printMipsCommand("slt", "$t" + counterTemporar, "$t" + "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.MIC_EGAL() != null) {
						printMipsCommand("sle", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					} else if (ctx.SI() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("and", "$t" + counterTemporar, "$t" + counterTemporar,
								"$t" + (counterTemporar + 1));
					} else if (ctx.SAU() != null) {
						printMipsCommand("sne", "$t" + counterTemporar, "$t" + counterTemporar, "$0");
						printMipsCommand("sne", "$t" + (counterTemporar + 1), "$t" + (counterTemporar + 1), "$0");
						printMipsCommand("or", "$t" + counterTemporar, "$t" + (counterTemporar + 1),
								"$t" + counterTemporar);
					}
				}
			}
		}
		return null;
	}

	public String getTipRecursiv(gramaticaParser.ExprContext ctx) {
		if (ctx.atom() != null) {
			return getTipAtom(ctx.atom());
		} else if (ctx.expr().size() == 1) {
			return getTipRecursiv(ctx.expr(0));
		} else if (ctx.expr().size() == 2) {
			String tip1 = getTipRecursiv(ctx.expr(0));
			String tip2 = getTipRecursiv(ctx.expr(1));
			if (tip1.equals(tip2)) {
				return tip1;
			}
		}
		return "invalid";
	}

	public boolean checkMatchingType(String tip1, String tip2) {
		return tip1.equals(tip2);
	}

	@Override
	public void enterDeclarareVariabila(gramaticaParser.DeclarareVariabilaContext ctx) {
		Variabila var = new Variabila();
		String variableType = ctx.TIP_DATA().toString();
		if (ctx.expr() != null) {
			gramaticaParser.ExprContext expr = ctx.expr();
			String tip = getTipRecursiv(expr);
			if (checkMatchingType(tip, variableType)) {
				if (!(ctx.getParent() instanceof gramaticaParser.Pentru_initializariContext)) {
					if (variabile.get(ctx.VARIABILA().toString()) == null) {
						var.setTip(variableType);
						var.setOffset(counter + "($sp)");
						variabile.put(ctx.VARIABILA().toString(), var);
					} else {
						System.err.println("Variabila deja declarata");
						error = true;
					}
				}
				if (!(ctx.getParent() instanceof gramaticaParser.Pentru_initializariContext)) {
					gramaticaParser.FolosireVariabilaContext variabilaFolosita = null;
					if (ctx.expr().atom() != null) {
						variabilaFolosita = ctx.expr().atom().folosireVariabila();
					}
					if (variabilaFolosita == null) {
						printMipsDeclarare(ctx.expr(), var);
					} else {
						printMipsDeclarare(ctx.expr(), variabile.get(variabilaFolosita.VARIABILA().toString()));
					}
				}
			} else {
				System.err.println("Tip invalid.");
				error = true;
			}
		} else

		{

			if (!(ctx.getParent() instanceof gramaticaParser.Pentru_initializariContext)) {
				var.setTip(variableType);
				var.setOffset(counter + "($sp)");
				if (variableType.equals("intreg")) {
					var.setByteNumber(4);
					counter += 4;
				} else if (variableType.equals("caracter")) {
					var.setByteNumber(1);
					counter += 1;
				} else if (variableType.equals("real")) {
					var.setByteNumber(8);
					counter += 8;
				}

				variabile.put(ctx.VARIABILA().toString(), var);
			}
		}
	}

	@Override
	public void exitDeclarareVariabila(gramaticaParser.DeclarareVariabilaContext ctx) {
		if (ctx.expr() != null && ctx.expr().atom() != null && ctx.expr().atom().apelareFunctie() != null) {
			printMipsAtribuire(counter + "($sp)", ctx.expr());
		}

	}

	@Override
	public void enterDeclarareFunctie(gramaticaParser.DeclarareFunctieContext ctx) {
		boolean isRef = false;
		if (ctx.AMPERSAND() != null) {
			isRef = true;
		}
		if (ctx.VARIABILA().size() == 2) {
			functii.put(ctx.VARIABILA(0).toString(),
					new FuncParams<>(ctx.TIP_DATA(1).toString(), ctx.TIP_DATA(0).toString(), false, isRef));
			Variabila var = new Variabila();
			String variableType = ctx.TIP_DATA(0).toString();
			var.setTip(variableType);
			var.setOffset("$a0");
			variabile.put(ctx.VARIABILA(1).toString(), var);
		} else {
			functii.put(ctx.VARIABILA(0).toString(), new FuncParams<>(ctx.TIP_DATA(0).toString(), "vid", false, isRef));
		}
		try {
			fileWriter.write(String.format("\n%s:\n", ctx.VARIABILA(0).toString()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void exitDeclarareFunctie(gramaticaParser.DeclarareFunctieContext ctx) {
		if (!functii.get(ctx.VARIABILA(0).toString()).funcType.equals("vid")
				&& functii.get(ctx.VARIABILA(0).toString()).isReturning == false) {
			System.err.println("Trebuia sa returnezi o valoare");
			error = true;
		}
		if (ctx.VARIABILA().size() == 2) {
			variabile.remove(ctx.VARIABILA(1).toString());
		}
		if (!error) {
			try {
				if (ctx.VARIABILA(0).toString().equals("main")) {
					fileWriter.write("\tj exit\n");
				} else if (functii.get(ctx.VARIABILA(0).toString()).funcType.equals("vid")) {
					fileWriter.write("\tjr $ra\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void enterApelareFunctie(gramaticaParser.ApelareFunctieContext ctx) {
		if (functii.get(ctx.VARIABILA().toString()) == null) {
			System.err.println("Functia nu a fost declarata");
			error = true;
		} else {
			String tipParam = functii.get(ctx.VARIABILA().toString()).paramType;
			if (!tipParam.equals("vid")) {
				if (ctx.expr() != null) {
					String tipDat = getTipRecursiv(ctx.expr());
					if (!checkMatchingType(tipParam, tipDat)) {
						System.err.println("Tipul valorii date nu este acelasi cu tipul parametrului");
						error = true;
					}
				} else {
					System.err.println("Functia necesita un parametru");
					error = true;
				}
			} else {
				if (ctx.expr() != null) {
					System.err.println("Functia nu are parametrii");
					error = true;
				}
			}
			if (functii.get(ctx.VARIABILA().toString()).paramByReference) {
				if (ctx.expr().atom() != null) {
					if (ctx.expr().atom().folosireVariabila() == null) {
						System.err.println("Functia primeste parametru lvalue");
						error = true;
					}
				} else {
					System.err.println("Functia primeste parametru lvalue");
					error = true;
				}
			}
		}
		if (!error) {
			try {
				if (!functii.get(ctx.VARIABILA().toString()).paramType.equals("vid")) {
					if (ctx.expr().atom() != null) {
						if (ctx.expr().atom().folosireVariabila() != null) {
							printMipsCommand("lw", "$a0", variabile
									.get(ctx.expr().atom().folosireVariabila().VARIABILA().toString()).getOffset());
						} else {
							saveValoareInT1(ctx.expr());
							printMipsCommand("move", "$a0", "$t1");
						}
					} else {
						saveValoareInT1(ctx.expr());
						printMipsCommand("move", "$a0", "$t1");
					}
				}
				fileWriter.write("\tjal " + ctx.VARIABILA().toString() + "\n");
				if (functii.get(ctx.VARIABILA().toString()).paramByReference) {
					printMipsCommand("sw", "$a0",
							variabile.get(ctx.expr().atom().folosireVariabila().VARIABILA().toString()).getOffset());
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}

	@Override
	public void exitApelareFunctie(gramaticaParser.ApelareFunctieContext ctx) {
	}

	@Override
	public void enterParsare(gramaticaParser.ParsareContext ctx) {
		try {
			fileWriter = new FileWriter("fisier.MIPS");
			fileWriter.write("\t.globl main\n");
			fileWriter.write("\tj main\n\n");
			fileWriter.write("exit:\n");
			fileWriter.write("\tli $v0, 10\n");
			fileWriter.write("\tsyscall\n");
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void exitParsare(gramaticaParser.ParsareContext ctx) {
		if (functii.get("main") == null) {
			System.err.println("Nu exista functia main");
			error = true;
		}
		try {
			fileWriter.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void enterBloc(gramaticaParser.BlocContext ctx) {

	}

	@Override
	public void exitBloc(gramaticaParser.BlocContext ctx) {
		for (gramaticaParser.LinieContext linie : ctx.linie()) {
			if (linie.declarareVariabila() != null) {
				variabile.remove(linie.declarareVariabila().VARIABILA().toString());
			}
		}
	}

	@Override
	public void enterLinie(gramaticaParser.LinieContext ctx) {
	}

	@Override
	public void exitLinie(gramaticaParser.LinieContext ctx) {
	}

	@Override
	public void enterAtribuire(gramaticaParser.AtribuireContext ctx) {
		String currentVariable = null;
		if (variabile.get(ctx.folosireVariabila().VARIABILA().toString()) != null) {
			currentVariable = variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getTip();
		}
		if (currentVariable != null) {
			if (ctx.expr() != null) {
				String tip = getTipRecursiv(ctx.expr());
				if (!(ctx.getParent() instanceof gramaticaParser.Pentru_initializariContext)
						&& !(ctx.getParent() instanceof gramaticaParser.Pentru_operatiiContext)) {
					if (!checkMatchingType(currentVariable, tip)) {
						System.err.println("Tipul variabilei " + ctx.folosireVariabila().VARIABILA().toString()
								+ " este diferit fata de expresie.");
						error = true;
					} else {
						if (ctx.expr().atom() != null) {
							if (!(ctx.getParent() instanceof gramaticaParser.Pentru_operatiiContext)
									&& ctx.expr().atom().apelareFunctie() == null) {
								printMipsAtribuire(
										variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getOffset(),
										ctx.expr());
							}
						} else {
							printMipsAtribuire(
									variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getOffset(),
									ctx.expr());
						}
					}
				}
			}
		}

	}

	@Override
	public void exitAtribuire(gramaticaParser.AtribuireContext ctx) {
		if (ctx.expr() != null) {
			if (ctx.expr().atom() != null) {
				if (ctx.expr().atom().apelareFunctie() != null) {
					printMipsAtribuire(variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getOffset(),
							ctx.expr());
				}
			}
		}
	}

	public void enterDaca_parametrii(gramaticaParser.Daca_parametriiContext ctx) {
		++counterEndIf;
		stackIf.push(counterEndIf);
	}

	@Override
	public void exitDaca_parametrii(gramaticaParser.Daca_parametriiContext ctx) {
		try {
			fileWriter.write(String.format("EndIf%d:\n", stackIf.peek()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		stackIf.pop();
	}

	public void printLabelElse(int number) {
		try {
			fileWriter.write(String.format("Else%d:\n", number));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enterBloc_conditie(gramaticaParser.Bloc_conditieContext ctx) {

	}

	@Override
	public void exitBloc_conditie(gramaticaParser.Bloc_conditieContext ctx) {
		gramaticaParser.Daca_parametriiContext parent = (gramaticaParser.Daca_parametriiContext) ctx.getParent();
		if (parent.bloc_conditie(parent.ALTFEL().size()) == ctx) {
			return;
		}
		try {
			fileWriter.write(String.format("\tj EndIf%d\n", counterEndIf));
			printLabelElse(stackElse.peek());
			stackElse.pop();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void printConditie(gramaticaParser.ExprContext ctx, String label) {
		saveValoareInT1(ctx);
		printMipsCommand("beq", "$t1", "$0", label);
	}

	@Override
	public void enterBloc_declaratii(gramaticaParser.Bloc_declaratiiContext ctx) {
		ParserRuleContext parent = ctx.getParent();
		if (parent instanceof gramaticaParser.Bloc_conditieContext) {
			gramaticaParser.Daca_parametriiContext parent2 = (gramaticaParser.Daca_parametriiContext) parent
					.getParent();
			if (parent2.bloc_conditie(parent2.ALTFEL().size()) != parent) {
				String label = String.format("Else%d", ++counterElse);
				stackElse.push(counterElse);
				printConditie(((gramaticaParser.Bloc_conditieContext) parent).expr(), label);
			} else if (parent2.bloc_declaratii() == null) {
				if (parent2.bloc_conditie(parent2.ALTFEL().size()) == parent) {
					String label = String.format("EndIf%d", counterEndIf);
					printConditie(((gramaticaParser.Bloc_conditieContext) parent).expr(), label);
				}
			} else {
				if (parent2.bloc_conditie(parent2.ALTFEL().size()) == parent) {
					String label = String.format("Else%d", ++counterElse);
					stackElse.push(counterElse);
					printConditie(((gramaticaParser.Bloc_conditieContext) parent).expr(), label);
				}
			}
		}

	}

	@Override
	public void exitBloc_declaratii(gramaticaParser.Bloc_declaratiiContext ctx) {

	}

	public void printLabelLoop(int number) {
		try {
			fileWriter.write(String.format("L%d:\n", number));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enterCat_timp_parametrii(gramaticaParser.Cat_timp_parametriiContext ctx) {
		printLabelLoop(++counterLoop);
		stackLoop.push(counterLoop);
		printConditie(ctx.expr(), String.format("EndLoop%d", counterLoop));
	}

	@Override
	public void exitCat_timp_parametrii(gramaticaParser.Cat_timp_parametriiContext ctx) {
		try {
			fileWriter.write(String.format("\tj L%d\n", stackLoop.peek()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		printEndLoop(stackLoop.peek());
		stackLoop.pop();
	}

	@Override
	public void enterExpr(gramaticaParser.ExprContext ctx) {
		if (ctx.SI() == null && ctx.SAU() == null) {
			if (ctx.expr().size() == 2) {
				String tip1 = getTipRecursiv(ctx.expr(0));
				String tip2 = getTipRecursiv(ctx.expr(1));
				if (!checkMatchingType(tip1, tip2)) {
					System.err.println("Expresia nu are acelasi tip pe ambele parti");
					error = true;
				}
			}
		}
	}

	@Override
	public void exitExpr(gramaticaParser.ExprContext ctx) {
	}

	@Override
	public void enterAtom(gramaticaParser.AtomContext ctx) {
	}

	@Override
	public void exitAtom(gramaticaParser.AtomContext ctx) {
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
	}

	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
	}

	@Override
	public void visitTerminal(TerminalNode node) {
	}

	@Override
	public void visitErrorNode(ErrorNode node) {
	}

	@Override
	public void enterReturn_line(gramaticaParser.Return_lineContext ctx) {
		ParserRuleContext current = ctx;
		String tip = getTipRecursiv(ctx.expr());
		while (current.getParent() != null) {
			if (current instanceof gramaticaParser.DeclarareFunctieContext) {
				functii.get(
						((gramaticaParser.DeclarareFunctieContext) current).VARIABILA(0).toString()).isReturning = true;
				if (!functii.get(((gramaticaParser.DeclarareFunctieContext) current).VARIABILA(0).toString()).funcType
						.equals(tip)) {
					System.err.println("Tip returnat invalid");
					error = true;
				}
				break;
			}
			current = current.getParent();
		}
	}

	@Override
	public void exitReturn_line(gramaticaParser.Return_lineContext ctx) {
		try {
			saveValoareInT1(ctx.expr());
			printMipsCommand("move", "$v0", "$t1");
			fileWriter.write("\tjr $ra\n");
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Override
	public void enterPentru_parametri(gramaticaParser.Pentru_parametriContext ctx) {

		Variabila var = new Variabila();

		for (gramaticaParser.Pentru_initializariContext cttx : ctx.pentru_initializari()) {
			if (cttx.declarareVariabila() != null) {
				String variableType;
				variableType = getTipRecursiv(cttx.declarareVariabila().expr());
				var.setTip(variableType);
				var.setOffset(counter + "($sp)");
				variabile.put(cttx.declarareVariabila().VARIABILA().toString(), var);
				printMipsAtribuire(counter + "($sp)", cttx.declarareVariabila().expr());
				if (variableType.equals("intreg")) {
					var.setByteNumber(4);
					counter += 4;
				} else if (variableType.equals("caracter")) {
					var.setByteNumber(1);
					counter += 1;
				} else if (variableType.equals("real")) {
					var.setByteNumber(8);
					counter += 8;
				}
			}
			if (cttx.atribuire() != null) {
				printMipsAtribuire(
						variabile.get(cttx.atribuire().folosireVariabila().VARIABILA().toString()).getOffset(),
						cttx.atribuire().expr());
			}
		}

		printLabelLoop(++counterLoop);
		stackLoop.push(counterLoop);
		printConditie(ctx.expr(0), String.format("EndLoop%d", counterLoop));

		int sizeInitializari = ctx.pentru_initializari().size() - 1;
		int sizeOperatii = ctx.pentru_operatii().size() - 1;

		if (sizeInitializari >= 0 && ctx.pentru_initializari().get(sizeInitializari).VIRGULA() != null) {
			System.err.println("Exista virgula dupa ultima initializare din PENTRU (L" + counterLoop + ")!\n");
			error = true;

		}

		if (sizeOperatii >= 0 && ctx.pentru_operatii().get(sizeOperatii).VIRGULA() != null) {
			System.err.println("Exista virgula dupa ultima operatie din PENTRU (L" + counterLoop + ")!\n");
			error = true;

		}

	}

	@Override
	public void exitPentru_parametri(gramaticaParser.Pentru_parametriContext ctx) {

		try {

			for (gramaticaParser.Pentru_operatiiContext cttx : ctx.pentru_operatii()) {
				if (cttx.atribuire() != null) {
					printMipsAtribuire(
							variabile.get(cttx.atribuire().folosireVariabila().VARIABILA().toString()).getOffset(),
							cttx.atribuire().expr());

				}

				if (cttx.incrementari_decrementari() != null) {
					printMipsIncrementariDecrementari(cttx.incrementari_decrementari());
				}

			}
			fileWriter.write(String.format("\tj L%d\n", stackLoop.peek()));

		} catch (IOException e) {
			e.printStackTrace();
		}
		printEndLoop(stackLoop.peek());
		stackLoop.pop();
		for (gramaticaParser.Pentru_initializariContext init : ctx.pentru_initializari()) {
			if (init.declarareVariabila() != null) {
				variabile.remove(init.declarareVariabila().VARIABILA().toString());
			}
		}
	}

	public void printEndLoop(int value) {
		try {
			fileWriter.write(String.format("EndLoop%d:\n", stackLoop.peek()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enterPentru_initializari(gramaticaParser.Pentru_initializariContext ctx) {

	}

	@Override
	public void exitPentru_initializari(gramaticaParser.Pentru_initializariContext ctx) {

	}

	public void printMipsIncrementariDecrementari(gramaticaParser.Incrementari_decrementariContext ctx) {

		if (variabile.containsKey(ctx.folosireVariabila().VARIABILA().toString())) {
			if (variabile.get(ctx.folosireVariabila().VARIABILA().toString()).equals("sir")) {
				System.err.println("Operatia nu poate fi realizata pe un sir");
				error = true;
			}
			if(!variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getOffset().equals("$a0")) {
				printMipsCommand("lw","$t" + ++counterTemporar, variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getOffset());
			}else {
				printMipsCommand("move","$t" + ++counterTemporar, variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getOffset());
			}
			printMipsCommand("li","$t" + ++counterTemporar, "1");
			if (ctx.INCREMENTARE() != null) {
				printMipsCommand("add","$t" + (counterTemporar - 1), "$t" + (counterTemporar - 1),"$t" + (counterTemporar));							
			}
			if (ctx.DECREMENTARE() != null) {
				printMipsCommand("sub","$t" + (counterTemporar - 1), "$t" + (counterTemporar - 1),"$t" + (counterTemporar));	
			}
			counterTemporar=0;
			printMipsCommand("move", "$s0" , "$t" + (counterTemporar + 1));
			printMipsCommand("sw","$s0", variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getOffset());
		}

	}

	@Override
	public void enterIncrementari_decrementari(gramaticaParser.Incrementari_decrementariContext ctx) {
		if (!(ctx.getParent() instanceof gramaticaParser.Pentru_operatiiContext)) {
			printMipsIncrementariDecrementari(ctx);
		}
	}

	@Override
	public void exitIncrementari_decrementari(gramaticaParser.Incrementari_decrementariContext ctx) {
	}

	@Override
	public void enterSelectare_parametri(gramaticaParser.Selectare_parametriContext ctx) {
		counterCase = 0;

		if (ctx.getParent().getParent() instanceof gramaticaParser.Bloc_caz_selectareContext) {

			try {
				fileWriter.write("\t");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (variabile.get(ctx.folosireVariabila().VARIABILA().toString()) != null) {
			String tip1 = variabile.get(ctx.folosireVariabila().VARIABILA().toString()).getTip();
			if (!(tip1.equals("intreg") || tip1.equals("caracter"))) {
				System.err.println("Selectarea permite doar tipul intreg sau caracter");
				error = true;
			} else {
				for (gramaticaParser.Bloc_caz_selectareContext blocCaz : ctx.bloc_caz_selectare()) {
					String tip2 = getTipRecursiv(blocCaz.expr());
					if (!checkMatchingType(tip1, tip2)) {
						System.err.println("Tipul cazului nu coincide cu tipul variabilei din selectare");
						error = true;
					}
				}
			}
		}

		stackSwitch.push(++counterSwitch);

	}

	@Override
	public void exitSelectare_parametri(gramaticaParser.Selectare_parametriContext ctx) {

		printEndSelectare(stackSwitch.pop());

	}

	public void printEndSelectare(int value) {

		try {
			fileWriter.write(String.format("EndSelectare%d:\n", value));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public Double resetCounterGetValoare(gramaticaParser.ExprContext ctx) {
		Double valoare = getValoareRecursiv(ctx);
		counterTemporar = 0;
		return valoare;
	}

	public void saveValoareInT1(gramaticaParser.ExprContext ctx) {
		Double valoare = resetCounterGetValoare(ctx);
		if (valoare != null) {
			printMipsCommand("li", "$t1", String.valueOf(valoare.intValue()));
		}
	}

	@Override
	public void enterBloc_caz_selectare(gramaticaParser.Bloc_caz_selectareContext ctx) {

		if (!error) {

			try {
				fileWriter.write(
						String.format("Selectare" + stackSwitch.peek().toString() + "Caz" + (++counterCase) + ":\n"));
				saveValoareInT1(ctx.expr());
				String jumpCase = ("Selectare" + ((Integer) counterSwitch).toString() + "Caz");

				gramaticaParser.Selectare_parametriContext switchParent = (gramaticaParser.Selectare_parametriContext) ctx
						.getParent();

				if (switchParent.bloc_caz_selectare().size() - 1 >= counterCase) {

					jumpCase += (counterCase + 1);
				}

				else if (switchParent.caz_implicit_selectare() != null) {
					jumpCase = "Selectare" + counterSwitch + "Implicit";

				} else {

					jumpCase = ("EndSelectare" + stackSwitch.peek());
				}
				printMipsCommand("lw", "$s0",
						variabile.get(switchParent.folosireVariabila().VARIABILA().toString()).getOffset());
				printMipsCommand("bne", "$s0", "$t1", jumpCase);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void exitBloc_caz_selectare(gramaticaParser.Bloc_caz_selectareContext ctx) {

		if (ctx.INTRERUPE() != null) {
			try {
				fileWriter.write("\tj EndSelectare" + stackSwitch.peek() + "\n");

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void enterCaz_implicit_selectare(gramaticaParser.Caz_implicit_selectareContext ctx) {

		try {
			fileWriter.write(String.format("Selectare%dImplicit:\n", stackSwitch.peek()));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void exitCaz_implicit_selectare(gramaticaParser.Caz_implicit_selectareContext ctx) {

		try {
			fileWriter.write(String.format("\tj EndSelectare" + stackSwitch.peek() + "\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enterPentru_operatii(gramaticaParser.Pentru_operatiiContext ctx) {

	}

	@Override
	public void exitPentru_operatii(gramaticaParser.Pentru_operatiiContext ctx) {

	}

	@Override
	public void enterExecuta_pana_cand_parametri(gramaticaParser.Executa_pana_cand_parametriContext ctx) {
		printLabelLoop(++counterLoop);
		stackLoop.push(counterLoop);

	}

	@Override
	public void exitExecuta_pana_cand_parametri(gramaticaParser.Executa_pana_cand_parametriContext ctx) {
		printConditie(ctx.expr(), String.format("L%d", stackLoop.peek()));
		stackLoop.pop();
	}

	@Override
	public void enterFolosireVariabila(gramaticaParser.FolosireVariabilaContext ctx) {
		if (variabile.get(ctx.VARIABILA().toString()) == null) {
			System.err.println("Variabila " + ctx.VARIABILA().toString() + " nu a fost declarata");
			error = true;
		}
	}

	@Override
	public void exitFolosireVariabila(gramaticaParser.FolosireVariabilaContext ctx) {

	}
}