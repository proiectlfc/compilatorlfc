class Variabila {
    private String tip = "vid";
    private String offset = null;

    private int byteNumber = 0;

    Variabila(String tip, String offset) {
        this.tip = tip;
        this.offset = offset;
    }

    Variabila()
    {

    }

    public void setByteNumber(int byteNumber) {
        this.byteNumber = byteNumber;
    }

    public int getByteNumber() {
        return byteNumber;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }
}