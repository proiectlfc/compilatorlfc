
class Atom{
	public Atom(Double valoare){
		this.valoare=valoare;
	}
	public Atom(String variabila){
		this.variabila=variabila;
	}
	
	private Double valoare = null;
	private String variabila=null;
	public Double getValoare() {
		return valoare;
	}
	public String getVariabila() {
		return variabila;
	}
}