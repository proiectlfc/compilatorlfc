

public class FuncParams<T1,T2>{
	public T1 funcType;
	public T1 paramType;
	public T2 isReturning;
	public T2 paramByReference;
	FuncParams(T1 first, T1 second,T2 third,T2 fourth){
		funcType = first;
		paramType = second;
		isReturning = third;
		paramByReference = fourth;
	}
}