// Generated from gramatica.g4 by ANTLR 4.9
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class gramaticaLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		INTREG=1, REAL=2, RETURN=3, SAU=4, SI=5, NU=6, EGAL=7, DIFERIT=8, MARE=9, 
		MIC=10, MARE_EGAL=11, MIC_EGAL=12, INMULTIRE=13, IMPARTIRE=14, PLUS_ATR=15, 
		INCREMENTARE=16, MINUS_ATR=17, DECREMENTARE=18, INMULTIRE_ATR=19, IMPARTIRE_ATR=20, 
		PROCENT=21, PUTERE=22, PLUS=23, MINUS=24, AMPERSAND=25, DIEZ=26, PARANTEZA_D=27, 
		PARANTEZA_I=28, ACOLADA_D=29, ACOLADA_I=30, DOUA_PUNCTE=31, PUNCT=32, 
		ATRIBUIRE=33, VIRGULA=34, PUNCT_VIRGULA=35, DACA=36, ALTFEL=37, PENTRU=38, 
		CAT_TIMP=39, EXECUTA=40, PANA_CAND=41, SELECTARE=42, CAZ=43, IMPLICIT=44, 
		INTRERUPE=45, FUNCTIE=46, COMENTARIU=47, SPATIU=48, TIP_DATA=49, VARIABILA=50, 
		CARACTER=51, SPACE=52, OTHER=53;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"INTREG", "REAL", "TIP_INT", "TIP_REAL", "TIP_CARACTER", "TIP_SIR", "TIP_VID", 
			"LITERA_MICA", "LITERA_MARE", "LITERA", "CIFRA", "CIFRA_NENULA", "SEMN", 
			"RETURN", "SAU", "SI", "NU", "EGAL", "DIFERIT", "MARE", "MIC", "MARE_EGAL", 
			"MIC_EGAL", "INMULTIRE", "IMPARTIRE", "PLUS_ATR", "INCREMENTARE", "MINUS_ATR", 
			"DECREMENTARE", "INMULTIRE_ATR", "IMPARTIRE_ATR", "PROCENT", "PUTERE", 
			"PLUS", "MINUS", "AMPERSAND", "DIEZ", "PARANTEZA_D", "PARANTEZA_I", "ACOLADA_D", 
			"ACOLADA_I", "DOUA_PUNCTE", "PUNCT", "ATRIBUIRE", "VIRGULA", "PUNCT_VIRGULA", 
			"DACA", "ALTFEL", "PENTRU", "CAT_TIMP", "EXECUTA", "PANA_CAND", "SELECTARE", 
			"CAZ", "IMPLICIT", "INTRERUPE", "FUNCTIE", "COMENTARIU", "SPATIU", "TIP_DATA", 
			"VARIABILA", "CARACTER", "SPACE", "OTHER"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "'returneaza'", "'SAU'", "'SI'", "'NU'", "'EGAL'", 
			"'DIFERIT'", "'>'", "'<'", "'>='", "'<='", "'*'", "'/'", "'+='", "'++'", 
			"'-='", "'--'", "'*='", "'/='", "'%'", "'^'", "'+'", "'-'", "'&'", "'#'", 
			"'('", "')'", "'{'", "'}'", "':'", "'.'", "'='", "','", "';'", "'DACA'", 
			"'ALTFEL'", "'PENTRU'", "'CAT_TIMP'", "'EXECUTA'", "'PANA_CAND'", "'SELECTARE'", 
			"'CAZ'", "'IMPLICIT'", "'INTRERUPE'", "'FUNCTIE'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "INTREG", "REAL", "RETURN", "SAU", "SI", "NU", "EGAL", "DIFERIT", 
			"MARE", "MIC", "MARE_EGAL", "MIC_EGAL", "INMULTIRE", "IMPARTIRE", "PLUS_ATR", 
			"INCREMENTARE", "MINUS_ATR", "DECREMENTARE", "INMULTIRE_ATR", "IMPARTIRE_ATR", 
			"PROCENT", "PUTERE", "PLUS", "MINUS", "AMPERSAND", "DIEZ", "PARANTEZA_D", 
			"PARANTEZA_I", "ACOLADA_D", "ACOLADA_I", "DOUA_PUNCTE", "PUNCT", "ATRIBUIRE", 
			"VIRGULA", "PUNCT_VIRGULA", "DACA", "ALTFEL", "PENTRU", "CAT_TIMP", "EXECUTA", 
			"PANA_CAND", "SELECTARE", "CAZ", "IMPLICIT", "INTRERUPE", "FUNCTIE", 
			"COMENTARIU", "SPATIU", "TIP_DATA", "VARIABILA", "CARACTER", "SPACE", 
			"OTHER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public gramaticaLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "gramatica.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\67\u01af\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t"+
		"=\4>\t>\4?\t?\4@\t@\4A\tA\3\2\5\2\u0085\n\2\3\2\3\2\7\2\u0089\n\2\f\2"+
		"\16\2\u008c\13\2\3\2\5\2\u008f\n\2\3\3\3\3\3\3\6\3\u0094\n\3\r\3\16\3"+
		"\u0095\3\3\3\3\5\3\u009a\n\3\3\3\3\3\7\3\u009e\n\3\f\3\16\3\u00a1\13\3"+
		"\5\3\u00a3\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3"+
		"\n\3\n\3\13\3\13\5\13\u00c8\n\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\21\3"+
		"\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\3"+
		"\31\3\31\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3"+
		"\36\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3"+
		"&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3"+
		"\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\62\3\62\3"+
		"\62\3\62\3\62\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\64\3\64\3"+
		"\64\3\64\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\65\3\65\3\65\3\65\3\65\3"+
		"\65\3\65\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\67\3\67\3"+
		"\67\3\67\38\38\38\38\38\38\38\38\38\39\39\39\39\39\39\39\39\39\39\3:\3"+
		":\3:\3:\3:\3:\3:\3:\3;\3;\3;\3;\3;\7;\u018c\n;\f;\16;\u018f\13;\3;\3;"+
		"\3<\3<\3<\3<\3=\3=\3=\3=\3=\5=\u019c\n=\3>\3>\3>\7>\u01a1\n>\f>\16>\u01a4"+
		"\13>\3?\3?\3?\3?\3@\3@\3@\3@\3A\3A\2\2B\3\3\5\4\7\2\t\2\13\2\r\2\17\2"+
		"\21\2\23\2\25\2\27\2\31\2\33\2\35\5\37\6!\7#\b%\t\'\n)\13+\f-\r/\16\61"+
		"\17\63\20\65\21\67\229\23;\24=\25?\26A\27C\30E\31G\32I\33K\34M\35O\36"+
		"Q\37S U!W\"Y#[$]%_&a\'c(e)g*i+k,m-o.q/s\60u\61w\62y\63{\64}\65\177\66"+
		"\u0081\67\3\2\13\3\2c|\3\2C\\\3\2\62;\3\2\63;\4\2--//\4\2\f\f\17\17\4"+
		"\2\13\f\"\"\3\2))\5\2\13\f\17\17\"\"\2\u01b2\2\3\3\2\2\2\2\5\3\2\2\2\2"+
		"\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2"+
		"\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2"+
		"\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2"+
		"\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M"+
		"\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2"+
		"\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2"+
		"\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s"+
		"\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\2\177"+
		"\3\2\2\2\2\u0081\3\2\2\2\3\u008e\3\2\2\2\5\u0090\3\2\2\2\7\u00a4\3\2\2"+
		"\2\t\u00ab\3\2\2\2\13\u00b0\3\2\2\2\r\u00b9\3\2\2\2\17\u00bd\3\2\2\2\21"+
		"\u00c1\3\2\2\2\23\u00c3\3\2\2\2\25\u00c7\3\2\2\2\27\u00c9\3\2\2\2\31\u00cb"+
		"\3\2\2\2\33\u00cd\3\2\2\2\35\u00cf\3\2\2\2\37\u00da\3\2\2\2!\u00de\3\2"+
		"\2\2#\u00e1\3\2\2\2%\u00e4\3\2\2\2\'\u00e9\3\2\2\2)\u00f1\3\2\2\2+\u00f3"+
		"\3\2\2\2-\u00f5\3\2\2\2/\u00f8\3\2\2\2\61\u00fb\3\2\2\2\63\u00fd\3\2\2"+
		"\2\65\u00ff\3\2\2\2\67\u0102\3\2\2\29\u0105\3\2\2\2;\u0108\3\2\2\2=\u010b"+
		"\3\2\2\2?\u010e\3\2\2\2A\u0111\3\2\2\2C\u0113\3\2\2\2E\u0115\3\2\2\2G"+
		"\u0117\3\2\2\2I\u0119\3\2\2\2K\u011b\3\2\2\2M\u011d\3\2\2\2O\u011f\3\2"+
		"\2\2Q\u0121\3\2\2\2S\u0123\3\2\2\2U\u0125\3\2\2\2W\u0127\3\2\2\2Y\u0129"+
		"\3\2\2\2[\u012b\3\2\2\2]\u012d\3\2\2\2_\u012f\3\2\2\2a\u0134\3\2\2\2c"+
		"\u013b\3\2\2\2e\u0142\3\2\2\2g\u014b\3\2\2\2i\u0153\3\2\2\2k\u015d\3\2"+
		"\2\2m\u0167\3\2\2\2o\u016b\3\2\2\2q\u0174\3\2\2\2s\u017e\3\2\2\2u\u0186"+
		"\3\2\2\2w\u0192\3\2\2\2y\u019b\3\2\2\2{\u019d\3\2\2\2}\u01a5\3\2\2\2\177"+
		"\u01a9\3\2\2\2\u0081\u01ad\3\2\2\2\u0083\u0085\5\33\16\2\u0084\u0083\3"+
		"\2\2\2\u0084\u0085\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u008a\5\31\r\2\u0087"+
		"\u0089\5\27\f\2\u0088\u0087\3\2\2\2\u0089\u008c\3\2\2\2\u008a\u0088\3"+
		"\2\2\2\u008a\u008b\3\2\2\2\u008b\u008f\3\2\2\2\u008c\u008a\3\2\2\2\u008d"+
		"\u008f\7\62\2\2\u008e\u0084\3\2\2\2\u008e\u008d\3\2\2\2\u008f\4\3\2\2"+
		"\2\u0090\u0091\5\3\2\2\u0091\u0093\5W,\2\u0092\u0094\5\27\f\2\u0093\u0092"+
		"\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0093\3\2\2\2\u0095\u0096\3\2\2\2\u0096"+
		"\u00a2\3\2\2\2\u0097\u0099\7g\2\2\u0098\u009a\5\33\16\2\u0099\u0098\3"+
		"\2\2\2\u0099\u009a\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009f\5\31\r\2\u009c"+
		"\u009e\5\27\f\2\u009d\u009c\3\2\2\2\u009e\u00a1\3\2\2\2\u009f\u009d\3"+
		"\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a3\3\2\2\2\u00a1\u009f\3\2\2\2\u00a2"+
		"\u0097\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\6\3\2\2\2\u00a4\u00a5\7k\2\2"+
		"\u00a5\u00a6\7p\2\2\u00a6\u00a7\7v\2\2\u00a7\u00a8\7t\2\2\u00a8\u00a9"+
		"\7g\2\2\u00a9\u00aa\7i\2\2\u00aa\b\3\2\2\2\u00ab\u00ac\7t\2\2\u00ac\u00ad"+
		"\7g\2\2\u00ad\u00ae\7c\2\2\u00ae\u00af\7n\2\2\u00af\n\3\2\2\2\u00b0\u00b1"+
		"\7e\2\2\u00b1\u00b2\7c\2\2\u00b2\u00b3\7t\2\2\u00b3\u00b4\7c\2\2\u00b4"+
		"\u00b5\7e\2\2\u00b5\u00b6\7v\2\2\u00b6\u00b7\7g\2\2\u00b7\u00b8\7t\2\2"+
		"\u00b8\f\3\2\2\2\u00b9\u00ba\7u\2\2\u00ba\u00bb\7k\2\2\u00bb\u00bc\7t"+
		"\2\2\u00bc\16\3\2\2\2\u00bd\u00be\7x\2\2\u00be\u00bf\7k\2\2\u00bf\u00c0"+
		"\7f\2\2\u00c0\20\3\2\2\2\u00c1\u00c2\t\2\2\2\u00c2\22\3\2\2\2\u00c3\u00c4"+
		"\t\3\2\2\u00c4\24\3\2\2\2\u00c5\u00c8\5\21\t\2\u00c6\u00c8\5\23\n\2\u00c7"+
		"\u00c5\3\2\2\2\u00c7\u00c6\3\2\2\2\u00c8\26\3\2\2\2\u00c9\u00ca\t\4\2"+
		"\2\u00ca\30\3\2\2\2\u00cb\u00cc\t\5\2\2\u00cc\32\3\2\2\2\u00cd\u00ce\t"+
		"\6\2\2\u00ce\34\3\2\2\2\u00cf\u00d0\7t\2\2\u00d0\u00d1\7g\2\2\u00d1\u00d2"+
		"\7v\2\2\u00d2\u00d3\7w\2\2\u00d3\u00d4\7t\2\2\u00d4\u00d5\7p\2\2\u00d5"+
		"\u00d6\7g\2\2\u00d6\u00d7\7c\2\2\u00d7\u00d8\7|\2\2\u00d8\u00d9\7c\2\2"+
		"\u00d9\36\3\2\2\2\u00da\u00db\7U\2\2\u00db\u00dc\7C\2\2\u00dc\u00dd\7"+
		"W\2\2\u00dd \3\2\2\2\u00de\u00df\7U\2\2\u00df\u00e0\7K\2\2\u00e0\"\3\2"+
		"\2\2\u00e1\u00e2\7P\2\2\u00e2\u00e3\7W\2\2\u00e3$\3\2\2\2\u00e4\u00e5"+
		"\7G\2\2\u00e5\u00e6\7I\2\2\u00e6\u00e7\7C\2\2\u00e7\u00e8\7N\2\2\u00e8"+
		"&\3\2\2\2\u00e9\u00ea\7F\2\2\u00ea\u00eb\7K\2\2\u00eb\u00ec\7H\2\2\u00ec"+
		"\u00ed\7G\2\2\u00ed\u00ee\7T\2\2\u00ee\u00ef\7K\2\2\u00ef\u00f0\7V\2\2"+
		"\u00f0(\3\2\2\2\u00f1\u00f2\7@\2\2\u00f2*\3\2\2\2\u00f3\u00f4\7>\2\2\u00f4"+
		",\3\2\2\2\u00f5\u00f6\7@\2\2\u00f6\u00f7\7?\2\2\u00f7.\3\2\2\2\u00f8\u00f9"+
		"\7>\2\2\u00f9\u00fa\7?\2\2\u00fa\60\3\2\2\2\u00fb\u00fc\7,\2\2\u00fc\62"+
		"\3\2\2\2\u00fd\u00fe\7\61\2\2\u00fe\64\3\2\2\2\u00ff\u0100\7-\2\2\u0100"+
		"\u0101\7?\2\2\u0101\66\3\2\2\2\u0102\u0103\7-\2\2\u0103\u0104\7-\2\2\u0104"+
		"8\3\2\2\2\u0105\u0106\7/\2\2\u0106\u0107\7?\2\2\u0107:\3\2\2\2\u0108\u0109"+
		"\7/\2\2\u0109\u010a\7/\2\2\u010a<\3\2\2\2\u010b\u010c\7,\2\2\u010c\u010d"+
		"\7?\2\2\u010d>\3\2\2\2\u010e\u010f\7\61\2\2\u010f\u0110\7?\2\2\u0110@"+
		"\3\2\2\2\u0111\u0112\7\'\2\2\u0112B\3\2\2\2\u0113\u0114\7`\2\2\u0114D"+
		"\3\2\2\2\u0115\u0116\7-\2\2\u0116F\3\2\2\2\u0117\u0118\7/\2\2\u0118H\3"+
		"\2\2\2\u0119\u011a\7(\2\2\u011aJ\3\2\2\2\u011b\u011c\7%\2\2\u011cL\3\2"+
		"\2\2\u011d\u011e\7*\2\2\u011eN\3\2\2\2\u011f\u0120\7+\2\2\u0120P\3\2\2"+
		"\2\u0121\u0122\7}\2\2\u0122R\3\2\2\2\u0123\u0124\7\177\2\2\u0124T\3\2"+
		"\2\2\u0125\u0126\7<\2\2\u0126V\3\2\2\2\u0127\u0128\7\60\2\2\u0128X\3\2"+
		"\2\2\u0129\u012a\7?\2\2\u012aZ\3\2\2\2\u012b\u012c\7.\2\2\u012c\\\3\2"+
		"\2\2\u012d\u012e\7=\2\2\u012e^\3\2\2\2\u012f\u0130\7F\2\2\u0130\u0131"+
		"\7C\2\2\u0131\u0132\7E\2\2\u0132\u0133\7C\2\2\u0133`\3\2\2\2\u0134\u0135"+
		"\7C\2\2\u0135\u0136\7N\2\2\u0136\u0137\7V\2\2\u0137\u0138\7H\2\2\u0138"+
		"\u0139\7G\2\2\u0139\u013a\7N\2\2\u013ab\3\2\2\2\u013b\u013c\7R\2\2\u013c"+
		"\u013d\7G\2\2\u013d\u013e\7P\2\2\u013e\u013f\7V\2\2\u013f\u0140\7T\2\2"+
		"\u0140\u0141\7W\2\2\u0141d\3\2\2\2\u0142\u0143\7E\2\2\u0143\u0144\7C\2"+
		"\2\u0144\u0145\7V\2\2\u0145\u0146\7a\2\2\u0146\u0147\7V\2\2\u0147\u0148"+
		"\7K\2\2\u0148\u0149\7O\2\2\u0149\u014a\7R\2\2\u014af\3\2\2\2\u014b\u014c"+
		"\7G\2\2\u014c\u014d\7Z\2\2\u014d\u014e\7G\2\2\u014e\u014f\7E\2\2\u014f"+
		"\u0150\7W\2\2\u0150\u0151\7V\2\2\u0151\u0152\7C\2\2\u0152h\3\2\2\2\u0153"+
		"\u0154\7R\2\2\u0154\u0155\7C\2\2\u0155\u0156\7P\2\2\u0156\u0157\7C\2\2"+
		"\u0157\u0158\7a\2\2\u0158\u0159\7E\2\2\u0159\u015a\7C\2\2\u015a\u015b"+
		"\7P\2\2\u015b\u015c\7F\2\2\u015cj\3\2\2\2\u015d\u015e\7U\2\2\u015e\u015f"+
		"\7G\2\2\u015f\u0160\7N\2\2\u0160\u0161\7G\2\2\u0161\u0162\7E\2\2\u0162"+
		"\u0163\7V\2\2\u0163\u0164\7C\2\2\u0164\u0165\7T\2\2\u0165\u0166\7G\2\2"+
		"\u0166l\3\2\2\2\u0167\u0168\7E\2\2\u0168\u0169\7C\2\2\u0169\u016a\7\\"+
		"\2\2\u016an\3\2\2\2\u016b\u016c\7K\2\2\u016c\u016d\7O\2\2\u016d\u016e"+
		"\7R\2\2\u016e\u016f\7N\2\2\u016f\u0170\7K\2\2\u0170\u0171\7E\2\2\u0171"+
		"\u0172\7K\2\2\u0172\u0173\7V\2\2\u0173p\3\2\2\2\u0174\u0175\7K\2\2\u0175"+
		"\u0176\7P\2\2\u0176\u0177\7V\2\2\u0177\u0178\7T\2\2\u0178\u0179\7G\2\2"+
		"\u0179\u017a\7T\2\2\u017a\u017b\7W\2\2\u017b\u017c\7R\2\2\u017c\u017d"+
		"\7G\2\2\u017dr\3\2\2\2\u017e\u017f\7H\2\2\u017f\u0180\7W\2\2\u0180\u0181"+
		"\7P\2\2\u0181\u0182\7E\2\2\u0182\u0183\7V\2\2\u0183\u0184\7K\2\2\u0184"+
		"\u0185\7G\2\2\u0185t\3\2\2\2\u0186\u0187\7\60\2\2\u0187\u0188\7\60\2\2"+
		"\u0188\u0189\7\60\2\2\u0189\u018d\3\2\2\2\u018a\u018c\n\7\2\2\u018b\u018a"+
		"\3\2\2\2\u018c\u018f\3\2\2\2\u018d\u018b\3\2\2\2\u018d\u018e\3\2\2\2\u018e"+
		"\u0190\3\2\2\2\u018f\u018d\3\2\2\2\u0190\u0191\b;\2\2\u0191v\3\2\2\2\u0192"+
		"\u0193\t\b\2\2\u0193\u0194\3\2\2\2\u0194\u0195\b<\2\2\u0195x\3\2\2\2\u0196"+
		"\u019c\5\7\4\2\u0197\u019c\5\t\5\2\u0198\u019c\5\13\6\2\u0199\u019c\5"+
		"\r\7\2\u019a\u019c\5\17\b\2\u019b\u0196\3\2\2\2\u019b\u0197\3\2\2\2\u019b"+
		"\u0198\3\2\2\2\u019b\u0199\3\2\2\2\u019b\u019a\3\2\2\2\u019cz\3\2\2\2"+
		"\u019d\u01a2\5\25\13\2\u019e\u01a1\5\25\13\2\u019f\u01a1\5\27\f\2\u01a0"+
		"\u019e\3\2\2\2\u01a0\u019f\3\2\2\2\u01a1\u01a4\3\2\2\2\u01a2\u01a0\3\2"+
		"\2\2\u01a2\u01a3\3\2\2\2\u01a3|\3\2\2\2\u01a4\u01a2\3\2\2\2\u01a5\u01a6"+
		"\7)\2\2\u01a6\u01a7\n\t\2\2\u01a7\u01a8\7)\2\2\u01a8~\3\2\2\2\u01a9\u01aa"+
		"\t\n\2\2\u01aa\u01ab\3\2\2\2\u01ab\u01ac\b@\2\2\u01ac\u0080\3\2\2\2\u01ad"+
		"\u01ae\13\2\2\2\u01ae\u0082\3\2\2\2\17\2\u0084\u008a\u008e\u0095\u0099"+
		"\u009f\u00a2\u00c7\u018d\u019b\u01a0\u01a2\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}